#include "qdbases.h"

QDBases::QDBases(QObject *parent) :
    QObject(parent), m_dbPort(0)
{    
    //qDebug() << Q_FUNC_INFO;
//    QDBases::connectToDb();
}

QDBases::~QDBases()
{
    closeConnection();
}

bool QDBases::connectToDb()
{
    if(!db.isValid()){
        db = QSqlDatabase::addDatabase(m_dbType);
    }
    db.setDatabaseName(m_dbName); 
    db.setHostName(m_hostName);
    db.setUserName(m_userName);
    db.setPassword(m_userPassword);
    db.setPort(m_dbPort);
    if (!db.open()) {
        qDebug() << "Cannot open database:" << db.lastError();
        return false;
    }    
    return true;
}

void QDBases::closeConnection()
{    
    db.close();
}

QStringList QDBases::listFromDb(QString queryString, int columnCount, QString delimeter)
{    
    QSqlQuery query;
    QStringList lst;

    if (!query.exec(queryString)) {
        if ((!db.open()) || (!query.exec(queryString))) {
            qDebug() << "Unable to execute query — exiting"<< query.lastError();
            lst << "Unable to execute query — exiting" ;
            return lst;
        }
    }

    while (query.next()) {
        QString str = query.value(0).toString();
        for (int i=1; i<columnCount; ++i){
            str += delimeter + query.value(i).toString();
        }
        lst << str;
    }
    return lst;
}

QVariantList QDBases::variantListFromDb(QString queryString)
{
    QSqlQuery query;
    QVariantList lst;
    QSqlRecord rec;
    if (!query.exec(queryString)) {
        if ((!db.open()) || (!query.exec(queryString))) {
            qDebug() << "Unable to execute query — exiting";
            //lst << "Unable to execute query — exiting";
            return lst;
        }
    }

    rec = query.record();
    int columnCount = rec.count();
    while (query.next()) {
        QStringList strList;
        for (int i=0; i<columnCount; ++i){
            strList << query.value(i).toString();
        }
        lst << strList;
    }
    return lst;
}

QString QDBases::hostName() const
{
    return m_hostName;
}
void QDBases::setHostName(const QString &host)
{
    if(m_hostName!=host){
        m_hostName = host;
        emit hostNameChanged();
    }
}
QString QDBases::dbName() const
{
    return m_dbName;
}
void QDBases::setDbName(const QString &dbname)
{
    if(m_dbName!=dbname){
        m_dbName = dbname;
        emit dbNameChanged();
    }
}
QString QDBases::userName() const
{
    return m_userName;
}
void QDBases::setUserName(const QString &user)
{
    if(m_userName!=user){
        m_userName = user;
        emit userNameChanged();
    }
}
QString QDBases::userPassword() const
{
    return m_userPassword;
}
void QDBases::setUserPassword(const QString &pass)
{
    if(m_userPassword!=pass){
        m_userPassword = pass;
        emit userPasswordChanged();
    }
}
int QDBases::dbPort() const
{
    return m_dbPort;
}
void QDBases::setDbPort(const int &port)
{
    if(m_dbPort!=port){
        m_dbPort = port;
        emit dbPortChanged();
    }
}
QString QDBases::dbType() const
{
    return m_dbType;
}
void QDBases::setDbType(const QString &dbtype)
{
    if(m_dbType!=dbtype){
        m_dbType = dbtype;
        emit dbTypeChanged();
    }
}
