import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2

Dialog {
    id: searchDialog
    signal fieldTextChanged(string role, string value)
    signal okAndClose()
    signal clearFields()
    title: qsTr("Поиск")
    contentItem: Rectangle {
        color: rootWindow.colorList.getColor("background")|| "steelblue"
        implicitWidth: uid.width + fio.width + rootWindow.margins * 3
        implicitHeight: uid.height * 3 + btnOk.height + labelHelp.height +
                        6*rootWindow.margins
        SearchField {
            id: build
            objectName: "address_build"
            width: 60
            font.pointSize: rootWindow.fontSize2
            placeholderText: qsTr("Дом")
            Keys.onPressed: oneKeyShortcut(event)
            anchors {
                margins: rootWindow.margins
                left: parent.left
                top: parent.top
            }
        }
        SearchField {
            id: flat
            objectName: "address_flat"
            width: 60
            font.pointSize: rootWindow.fontSize2
            placeholderText: qsTr("Квартира")
            Keys.onPressed: oneKeyShortcut(event)
            anchors {
                margins: rootWindow.margins
                left: build.right
                top: parent.top
            }
        }
        SearchField {
            id: street
            objectName: "address_street"
            width: 175
            font.pointSize: rootWindow.fontSize2
            placeholderText: qsTr("Улица")
            Keys.onPressed: oneKeyShortcut(event)
            anchors {
                margins: rootWindow.margins
                left: flat.right
                top: parent.top
            }
        }
        SearchField {
            id: fio
            objectName: "fio"
            width: 250
            font.pointSize: rootWindow.fontSize2
            placeholderText: qsTr("ФИО")
            Keys.onPressed: oneKeyShortcut(event)
            anchors {
                margins: rootWindow.margins
                left: parent.left
                top: build.bottom
            }
        }
        SearchField {
            id: uid
            objectName: "uid"
            width: 50
            font.pointSize: rootWindow.fontSize2
            placeholderText: qsTr("Л/С")
            Keys.onPressed: oneKeyShortcut(event)
            anchors {
                margins: rootWindow.margins
                left: fio.right
                top: build.bottom
            }
        }        
        SearchField {
            id: city
            objectName: "city"
            width: 140
            font.pointSize: rootWindow.fontSize2
            placeholderText: qsTr("Город")
            Keys.onPressed: oneKeyShortcut(event)
            anchors {
                margins: rootWindow.margins
                left: parent.left
                top: fio.bottom
            }
        }        
        SearchField {
            id: dogovor
            objectName: "contract_id"
            width: 70
            font.pointSize: rootWindow.fontSize2
            placeholderText: qsTr("Договор")
            Keys.onPressed: oneKeyShortcut(event)
            anchors {
                margins: rootWindow.margins
                left: city.right
                top: fio.bottom
            }
        }
        SearchField {
            id: dateDogovor
            objectName: "contract_date"
            width: 85
            font.pointSize: rootWindow.fontSize2
            placeholderText: qsTr("Дата договора")
            Keys.onPressed: oneKeyShortcut(event)
            anchors {
                margins: rootWindow.margins
                left: dogovor.right
                top: fio.bottom
            }
        }
        Button {
            id: btnOk
            action: okAction
            anchors {
                margins: rootWindow.margins
                right: btnCancel.left
                top: dateDogovor.bottom
            }            
        }
        Button {
            id: btnCancel
            action: cancelAction            
            Keys.onReturnPressed: cancelAction.trigger()
            Keys.onEnterPressed: cancelAction.trigger()
            anchors {
                margins: rootWindow.margins
                horizontalCenter: parent.horizontalCenter
//                left: parent.horizontalCenter
                top: dateDogovor.bottom
            }
        }
        Button {
            id: btnClear
            action: clearAction
            Keys.onReturnPressed: clearAction.trigger()
            Keys.onEnterPressed: clearAction.trigger()
            anchors {
                margins: rootWindow.margins
                left: btnCancel.right
                top: dateDogovor.bottom
            }
        }
        Label {
            id: labelHelp
            text: qsTr("Для ввода знака \"" + clearAction.shortcut + "\" удерживать Ctrl или Alt")
            anchors {
                margins: rootWindow.margins
                top: btnOk.bottom
                horizontalCenter: parent.horizontalCenter
            }
            font.pointSize: 8
        }

        Action {
            id: okAction
            enabled: searchDialog.visible
            text: qsTr("Ок") + " \"" + shortcut + "\""
            shortcut: ((!btnCancel.activeFocus)&&(!btnClear.activeFocus))?"Enter":""
            tooltip: qsTr("Начать поиск")
            onTriggered: {
                searchDialog.visible = false
                okAndClose()
            }
        }
        Action {
            id: ok2Action
            enabled: searchDialog.visible
            shortcut: ((!btnCancel.activeFocus)&&(!btnClear.activeFocus))?"return":""
            onTriggered: {
                okAction.trigger()
            }
        }
        Action {
            id: cancelAction
            enabled: searchDialog.visible
            text: qsTr("Отмена") + " \"" + shortcut + "\""
            shortcut: "Esc"
            tooltip: qsTr("Отменить поиск")
            onTriggered: {                
                searchDialog.clearFields()
                searchDialog.visible = false
            }
        }
        Action {
            id: clearAction
            enabled: searchDialog.visible
            text: qsTr("Очистить") + " \"" + shortcut + "\""
            shortcut: "-"
            tooltip: qsTr("Очистить все поля")
            onTriggered: {
                searchDialog.clearFields()
            }
        }
    }

    //in Qt version greater than 5.4 do not work one-key shortcut in text field
    function oneKeyShortcut(event)
    {
        if((event.text == clearAction.shortcut) && !(event.modifiers & Qt.ShiftModifier) && !(event.modifiers & Qt.ControlModifier)
                & !(event.modifiers & Qt.AltModifier)){
            clearAction.trigger()
            event.accepted = true;
        }
    }

    onClearFields: {
        uid.text = ""
        fio.text = ""
        city.text = ""
        street.text = ""
        build.text = ""
        flat.text = ""
        dogovor.text = ""
        dateDogovor.text = ""
    }
    Component.onCompleted: {
        uid.fieldTextChanged.connect(searchDialog.fieldTextChanged)
        fio.fieldTextChanged.connect(searchDialog.fieldTextChanged)
        city.fieldTextChanged.connect(searchDialog.fieldTextChanged)
        street.fieldTextChanged.connect(searchDialog.fieldTextChanged)
        build.fieldTextChanged.connect(searchDialog.fieldTextChanged)
        flat.fieldTextChanged.connect(searchDialog.fieldTextChanged)
        dogovor.fieldTextChanged.connect(searchDialog.fieldTextChanged)
        dateDogovor.fieldTextChanged.connect(searchDialog.fieldTextChanged)
    }
    onVisibleChanged: {
        if(visible){
            build.forceActiveFocus()
        }
    }

}

