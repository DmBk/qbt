import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1

Dialog {
    id: paymentsSearchDialog    
    title: qsTr("Поиск платежей")
    width: 300
    height: label1.height*3 + rowDate.height + rowRegDate.height + row3.height*6
            + rootWindow.margins*9 + labelHelp.height
    property bool firstRun: true
    signal fieldTextChanged(string role, string value, bool isDate)
    signal okAndClose()
    signal clearFields()    
    contentItem: Rectangle {
        color: rootWindow.colorList.getColor("background")|| "steelblue"
        focus: true;
        Label {
            id: label1
            text: qsTr("Дата платежа")
            font.pointSize: rootWindow.fontSize2
            anchors {
                top: parent.top
                horizontalCenter: parent.horizontalCenter
                margins: rootWindow.margins
            }
        }
        RowLayout {
            id: rowDate
            anchors {
                top: label1.bottom
                horizontalCenter: parent.horizontalCenter
                margins: rootWindow.margins
            }
            CheckBox {
                id: dateCheckBox
                onCheckedChanged: {
                    if(checked){
                        fieldTextChanged("date", betweenDate(calendarDateBegin.displayText, calendarDateEnd.displayText), true)
                    } else {
                        fieldTextChanged("date", "", true)
                    }
                }
            }
            CalendarBox {
                id: calendarDateBegin
                enabled: dateCheckBox.checked
                calendarX: paymentsSearchDialog.x + rowDate.x + calendarDateBegin.x
                calendarY: paymentsSearchDialog.y + rowDate.y + calendarDateBegin.y
                Keys.onPressed: oneKeyShortcut(event)
                onDisplayTextChanged: {
                    if(acceptableInput)
                    fieldTextChanged("date", betweenDate(calendarDateBegin.displayText, calendarDateEnd.displayText), true)
                }
            }
            Label {
                text: "-"
            }
            CalendarBox {
                id: calendarDateEnd
                enabled: dateCheckBox.checked
                calendarX: paymentsSearchDialog.x + rowDate.x + calendarDateEnd.x
                calendarY: paymentsSearchDialog.y + rowDate.y + calendarDateEnd.y
                Keys.onPressed: oneKeyShortcut(event)
                onDisplayTextChanged: {
                    if(acceptableInput)
                    fieldTextChanged("date", betweenDate(calendarDateBegin.displayText, calendarDateEnd.displayText), true)
                }
            }            
        }
        Label {
            id: label2
            text: qsTr("Дата регистрации платежа")
            font.pointSize: rootWindow.fontSize2
            anchors {
                top: rowDate.bottom
                horizontalCenter: parent.horizontalCenter
                margins: rootWindow.margins
            }
        }
        RowLayout {
            id: rowRegDate
            anchors {
                top: label2.bottom
                horizontalCenter: parent.horizontalCenter
                margins: rootWindow.margins
            }
            CheckBox {
                id: regDateCheckBox                
                onCheckedChanged: {
                    if(checked){
                        fieldTextChanged("reg_date", betweenDate(calendarRegDateBegin.displayText, calendarRegDateEnd.displayText), true)
                    } else {
                        fieldTextChanged("reg_date", "", true)
                    }
                }
            }
            CalendarBox {
                id: calendarRegDateBegin
                enabled: regDateCheckBox.checked
                calendarX: paymentsSearchDialog.x + rowRegDate.x + calendarRegDateBegin.x
                calendarY: paymentsSearchDialog.y + rowRegDate.y + calendarRegDateBegin.y
                Keys.onPressed: oneKeyShortcut(event)
                onDisplayTextChanged: {
                    if(acceptableInput)
                    fieldTextChanged("reg_date", betweenDate(calendarRegDateBegin.displayText, calendarRegDateEnd.displayText), true)
                }
            }
            Label {
                text: "-"
            }
            CalendarBox {
                id: calendarRegDateEnd
                enabled: regDateCheckBox.checked
                calendarX: paymentsSearchDialog.x + rowRegDate.x + calendarRegDateEnd.x
                calendarY: paymentsSearchDialog.y + rowRegDate.y + calendarRegDateEnd.y
                Keys.onPressed: oneKeyShortcut(event)
                onDisplayTextChanged: {
                    if(acceptableInput)
                    fieldTextChanged("reg_date", betweenDate(calendarRegDateBegin.displayText, calendarRegDateEnd.displayText), true)
                }
            }            
        }
        RowLayout {
            id: row3
            anchors {
                top: rowRegDate.bottom
                horizontalCenter: parent.horizontalCenter
                margins: rootWindow.margins
            }
            TextField {
                id: admin
                implicitWidth: 100
                placeholderText: qsTr("Администратор")
//                focus: true
                Keys.onPressed: oneKeyShortcut(event)
                onDisplayTextChanged: {
                    fieldTextChanged("name", displayText, false)
                }
            }
            TextField {
                id: pack
//                objectName: "pack"
                implicitWidth: 65
                placeholderText: qsTr("Пачка")
                Keys.onPressed: oneKeyShortcut(event)
                onDisplayTextChanged: {
                    fieldTextChanged("inner_describe", innerDescribe(), false)
                }
            }
            TextField {
                id: uid
//                objectName: "uid"
                implicitWidth: 55
                placeholderText: qsTr("Л/С")
                Keys.onPressed: oneKeyShortcut(event)
                onDisplayTextChanged: {
                    fieldTextChanged("uid", displayText, false)
                }
            }
            TextField {
                id: sum
//                objectName: "sum"
                implicitWidth: 55
                placeholderText: qsTr("Сумма")
                Keys.onPressed: oneKeyShortcut(event)
                onDisplayTextChanged: {
                    fieldTextChanged("sum", displayText, false)
                }
            }
        }
        RowLayout {
            id: row4
            anchors {
                top: row3.bottom
                horizontalCenter: parent.horizontalCenter
                margins: rootWindow.margins
            }
            Label {
                id: descLabel
                text: qsTr("Описание:  ")
            }

            TextField {
                id: desc
//                objectName: "dsc"
                implicitWidth: 300 - rootWindow.margins*3 - descLabel.implicitWidth
//                placeholderText: qsTr("Описание")
                Keys.onPressed: oneKeyShortcut(event)
                onDisplayTextChanged: {
                    fieldTextChanged("dsc", displayText, false)
                }
            }
        }
        RowLayout {
            id: row5
            anchors {
                top: row4.bottom
                horizontalCenter: parent.horizontalCenter
                margins: rootWindow.margins
            }            
            Label {
                id: innerDescLabel
                text: qsTr("Внутреннее:")
            }
            ComboBox {
                id: innerDescribeBox
                editable: true
                activeFocusOnPress: true
                implicitWidth: row4.implicitWidth - innerDescLabel.implicitWidth - row5.spacing
                model: rootWindow.arrInnerDescribe
                Keys.onPressed: oneKeyShortcut(event)
                onEditTextChanged: {
                    if(!firstRun){
                        fieldTextChanged("inner_describe", innerDescribe(), false)
                    } else {
                        firstRun = false
                    }

                }
            }
        }
        RowLayout {
            id: row6
            anchors {
                top: row5.bottom
                horizontalCenter: parent.horizontalCenter
                margins: rootWindow.margins
            }
            TextField {
                id: build
                implicitWidth: 40
                placeholderText: qsTr("Дом")
                Keys.onPressed: oneKeyShortcut(event)
                onDisplayTextChanged: {
                    fieldTextChanged("address_build", displayText, false)
                }
            }
            TextField {
                id: flat
                implicitWidth: 40
                placeholderText: qsTr("Квартира")
                Keys.onPressed: oneKeyShortcut(event)
                onDisplayTextChanged: {
                    fieldTextChanged("address_flat", displayText, false)
                }
            }
            TextField {
                id: street
                implicitWidth: 100
                placeholderText: qsTr("Улица")
                Keys.onPressed: oneKeyShortcut(event)
                onDisplayTextChanged: {
                    fieldTextChanged("address_street", displayText, false)
                }
            }
            TextField {
                id: city
                implicitWidth: 95
                placeholderText: qsTr("Город")
                Keys.onPressed: oneKeyShortcut(event)
                onDisplayTextChanged: {
                    fieldTextChanged("city", displayText, false)
                }
            }
        }

        RowLayout {
            id: row7
            anchors {
                top: row6.bottom
                horizontalCenter: parent.horizontalCenter
                margins: rootWindow.margins
            }
            CheckBox {
                id: companyCheck
                onCheckedChanged: {
                    if(checked){
                        fieldTextChanged("name_account", companyBox.currentText, false)
                    } else {
                        fieldTextChanged("name_account", "", false)
                    }
                }
            }
            ComboBox {
                id: companyBox
                enabled: companyCheck.checked
                model: [qsTr("Неопределено"), qsTr("Артнет"), qsTr("Артнет Плюс"), qsTr("Артмедиа")]
                implicitWidth: 100                
                onCurrentIndexChanged: {
                    if(companyCheck.checked)
                    fieldTextChanged("name_account", currentText, false)
                }
            }
            CheckBox {
                id: methodChek
                onCheckedChanged: {
                    if(checked){
                        fieldTextChanged("method",(rootWindow.paymentMethodModel.get(methodBox.currentIndex).num).toString(), false)
                    } else {
                        fieldTextChanged("method","", false)
                    }
                }
            }
            ComboBox {
                id: methodBox
                enabled: methodChek.checked
                implicitWidth: 300 - rootWindow.margins*5 - companyCheck.implicitWidth -
                               companyBox.implicitWidth - methodChek.implicitWidth
                model: rootWindow.paymentMethodModel
//                model: ListModel {
//                    id: methodModel
//                    ListElement { text: qsTr("Банк"); num: 0 }
//                    ListElement { text: qsTr("Корректировка"); num: 5 }
//                    ListElement { text: qsTr("Компенсация"); num: 6 }
//                    ListElement { text: "Credit Card"; num: 3 }
//                    ListElement { text: "IPay"; num: 72 }
//                    ListElement { text: "WebMoney"; num: 41 }
//                    ListElement { text: qsTr("Внешние платежи"); num: 2 }
//                    ListElement { text: qsTr("Наличные"); num: 1 }
//                    ListElement { text: qsTr("Перевод личных средств"); num: 7 }
//                    ListElement { text: qsTr("Пересчитать"); num: 8 }
//                    ListElement { text: qsTr("Бонус"); num: 4 }
//                }                
                onCurrentIndexChanged: {                    
                    if(methodChek.checked)
                    fieldTextChanged("method",(rootWindow.paymentMethodModel.get(methodBox.currentIndex).num).toString(), false)
                }
            }
        }
        Button {
            id: btnOk
            action: okAction
            anchors {
                margins: rootWindow.margins
                right: btnCancel.left
                top: row7.bottom
            }
        }
        Button {
            id: btnCancel
            action: cancelAction
            Keys.onReturnPressed: cancelAction.trigger()
            Keys.onEnterPressed: cancelAction.trigger()
            anchors {
                margins: rootWindow.margins
                horizontalCenter: parent.horizontalCenter
                top: row7.bottom
            }
        }
        Button {
            id: btnClear
            action: clearAction
            Keys.onReturnPressed: clearAction.trigger()
            Keys.onEnterPressed: clearAction.trigger()
            anchors {
                margins: rootWindow.margins
                left: btnCancel.right
                top: row7.bottom
            }
        }
        Label {
            id: labelHelp
            text: qsTr("Для ввода знака \"" + clearAction.shortcut + "\" удерживать Ctrl или Alt")
            anchors {
                margins: rootWindow.margins
                top: btnOk.bottom
                horizontalCenter: parent.horizontalCenter
            }
            font.pointSize: 8
        }
        Action {
            id: okAction
            enabled: paymentsSearchDialog.visible
            text: qsTr("Ок") + " \"" + shortcut + "\""
            shortcut: ((!btnCancel.activeFocus)&&(!btnClear.activeFocus))?"Enter":""
            tooltip: qsTr("Поиск")
            onTriggered: {
                paymentsSearchDialog.visible = false
                okAndClose()
            }
        }
        Action {
            id: ok2Action
            enabled: paymentsSearchDialog.visible
            shortcut: ((!btnCancel.activeFocus)&&(!btnClear.activeFocus))?"return":""
            onTriggered: {
                okAction.trigger()
            }
        }
        Action {
            id: cancelAction
            enabled: paymentsSearchDialog.visible
            text: qsTr("Отмена") + " \"" + shortcut + "\""
            shortcut: "Esc"
            tooltip: qsTr("Отменить поиск")
            onTriggered: {
                paymentsSearchDialog.clearFields()
                paymentsSearchDialog.visible = false
            }
        }
        Action {
            id: clearAction            
            enabled: paymentsSearchDialog.visible            
            text: qsTr("Очистить") + " \"" + shortcut + "\""
            shortcut: "ctrl+-"
            tooltip: qsTr("Очистить все поля")
            onTriggered: {
                paymentsSearchDialog.clearFields()
//                console.log("-")
            }
        }
    }

    onClearFields: {
//        dateCheckBox.checked = false
//        regDateCheckBox.checked = false
        admin.text = ""
        pack.text = ""
        uid.text = ""
        sum.text = ""
        desc.text = ""
        build.text = ""
        flat.text = ""
        street.text = ""
        city.text = ""
        innerDescribeBox.currentIndex = 0
        companyCheck.checked = false
//        companyBox.currentIndex = 0
        methodChek.checked = false
//        methodBox.currentIndex = 0
    }
    Component.onCompleted: {
        regDateCheckBox.checked = true
    }

    //in Qt version greater than 5.4 do not work one-key shortcut in text field
    function oneKeyShortcut(event)
    {
        //        console.debug("Keys.OnPressed", event.key.toString(), event.text)
        if((event.text == clearAction.shortcut) && !(event.modifiers & Qt.ShiftModifier) && !(event.modifiers & Qt.ControlModifier)
                & !(event.modifiers & Qt.AltModifier)){
            //            console.debug("event.key === Qt.Key_Control", Qt.Key_Minus)
            clearAction.trigger()
            event.accepted = true;
        }
    }

    function betweenDate(beginDate, endDate)
    {
        return "BETWEEN '" + beginDate + " 00:00:00' AND '" + endDate + " 23:59:59'"
    }

    function innerDescribe()
    {
        var str = innerDescribeBox.editText.trim() +
                (pack.displayText.length>0?"%" + rootWindow.packAlias + pack.displayText:"%")
        return (str!="%"?str:"")
    }
}
