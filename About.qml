import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1

Dialog {
    id: about
    title: qsTr("О программе")
    width: layot.implicitWidth + 4*rootWindow.margins
    height: layot.implicitHeight + 4*rootWindow.margins
    contentItem: Rectangle {
        color: rootWindow.colorList.getColor("background")|| "steelblue"
        ColumnLayout {
            id: layot
            anchors.centerIn: parent
            Label {
                id: label1
                text: "Billing Tools"
                font.pointSize: rootWindow.fontSizeTop
                font.bold: true
                anchors {
                    horizontalCenter: parent.horizontalCenter
                }
            }
            Label {
                id: label2
                text: qsTr("Утилиты для системы биллинга Abills")
                font.pointSize: rootWindow.fontSize1
            }
            Label {
                id: label3
                text: qsTr("Версия 1.0.8 от 2016-11-14")
                font.pointSize: rootWindow.fontSize2
            }
            Label {
                id: label4
                text: qsTr("Разработчик: Дмитрий Бибик")
                font.pointSize: rootWindow.fontSize1
            }
            Text {
                id: label5
                text: qsTr("email: dm.mailbox1@gmail.com")
                font.pointSize: rootWindow.fontSize2
            }
        }
    }
}
