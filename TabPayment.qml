import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
//import QtQuick.Controls.Styles 1.4
//import QtQuick.Layouts 1.1
import PsSqlQueryModel 0.7

Item {
    id: itemPayment
    objectName: "objTabPayment"
    signal searchOkAndClose()
    PsTableView {
        id: tableView
        signal filterChanged(string role, string value)
        signal searchChanged(string role, string value)
        signal clearSearch()
        anchors {
            top: parent.top
            right: parent.right
            left: parent.left
            bottom: searchBar.visible? searchBar.top:parent.bottom            
        }
        TableViewColumn { role: "uid"; title: qsTr("Л/С"); width: 45 }
        TableViewColumn { role: "fio"; title: qsTr("ФИО"); width: 180 }
        TableViewColumn { role: "city"; title: qsTr("Город"); width: 90 }
        TableViewColumn { role: "address_street"; title: qsTr("Улица"); width: 90 }
        TableViewColumn { role: "address_build"; title: qsTr("Дом"); width: 35 }
        TableViewColumn { role: "address_flat"; title: qsTr("Квартира"); width: 35 }
        TableViewColumn { role: "contract_id"; title: qsTr("Договор"); width: 60 }
        //        Keys.onPressed: {
        //            console.debug("Keys.OnPressed")
        //            if(event.key===Qt.Key_Control){
        //                console.debug("event.key===Qt.Key_Control")
        //            }
        //        }

        model: PsSqlQueryModel {
            id: tableModel
            selectString: "SELECT users_pi.*, users.disable, users.deleted, bills.deposit FROM users_pi
                           LEFT JOIN users ON users_pi.uid=users.uid
                           LEFT JOIN bills ON users.bill_id=bills.id "            
            strictFilter: true
        }

        itemDelegate: Text {
//            width: parent.width
            anchors.margins: 4
//            anchors.left: parent.left
//            anchors.verticalCenter: parent.verticalCenter
            elide: Text.ElideRight
            clip: true
            font.pointSize: rootWindow.fontSize3
            text: if (styleData.value instanceof Date)
                      if(styleData.role=="contract_date")
                          styleData.value.toLocaleString(Qt.locale(), "yyyy-MM-dd")
                      else styleData.value.toLocaleString(Qt.locale(), "yyyy-MM-dd HH:mm:ss")
                  else if(styleData.role == "deposit")
                                        Number(styleData.value).toLocaleString(Qt.locale()) || "0"
                  else styleData.value
            color: styleData.textColor
        }

        rowDelegate: Rectangle {
            id: tableViewDelegate
            height: 16
            color: {
                var disable
                var deleted
                if((styleData.row !== undefined) && (styleData.row > -1)){
                disable = tableModel.itemFromRow(styleData.row, "disable")
                deleted = tableModel.itemFromRow(styleData.row, "deleted")
                } else {
                    disable = 0
                    deleted = 0
                }

                if(!styleData.selected){
                    if(disable === 0 && deleted === 0)
                        styleData.alternate && tableView.alternatingRowColors?
                                    rootWindow.colorList.getColor("tableViewAlternatingRow"):
                                    rootWindow.colorList.getColor("tableViewRow")
                    else if (deleted === 0)
                        rootWindow.colorList.getColor("tableViewDisableRow")
                    else
                        rootWindow.colorList.getColor("tableViewDeletedRow")
                    //                           "Wheat"
                    //                           "coral"
                    //                           "Tan"
                } else {
                    if(disable === 0 && deleted === 0)
                        rootWindow.colorList.getColor("tableViewSelectedRow")
                    else if(deleted === 0)
                        rootWindow.colorList.getColor("tableViewSelectedDisableRow")
                    else
                        rootWindow.colorList.getColor("tableViewSelectedDeletedRow")
                    //                        "Navy"
                    //                        "steelblue"
                }
            }
        }
        Component.onCompleted: {            
            tableModel.setMultiRole("uid", "users_pi")
            tableModel.setFullQuery()
            psAddColumn("contract_date","Дата договора", 70)
            psAddColumn("deposit","Депозит", 50)            
            searchBar.fieldTextChanged.connect(tableView.filterChanged)
            searchDialog.fieldTextChanged.connect(tableView.searchChanged)
            addPaymentDialog.refreshPayment.connect(tableModel.setFullQuery)                        
            tableView.forceActiveFocus()
        }

        onSortIndicatorOrderChanged: {
            timer.stop()

            if(!tableView.sortIndicatorVisible) {
                tableView.sortIndicatorVisible = true
            }

            var column = tableView.getColumn(tableView.sortIndicatorColumn)
            tableModel.setSortRoles(column.role, tableView.sortIndicatorOrder)
            tableModel.setFullQuery();

        }

        onSortIndicatorColumnChanged: {
            timer.stop()

            if(!tableView.sortIndicatorVisible) {
                tableView.sortIndicatorVisible = true
            }

            var column = tableView.getColumn(tableView.sortIndicatorColumn)
            tableModel.setSortRoles(column.role, tableView.sortIndicatorOrder)
            tableModel.setFullQuery();
        }

        onSearchChanged: {
            tableModel.setSearchRoles(role, value)
            tableModel.setFullQuery()
        }

        onFilterChanged: {
            tableModel.setFilterRoles(role, value)
            tableModel.setFullQuery()
        }

        onRowCountChanged: {
            statusWrite()
        }

//        onColumnsWidthChanged: {
//            //            console.debug("onColumnsWidthChanged=", columnsWidth)
//        }

        onHorizontalScrollBarChanged: {
            //            console.debug("onHorizontalScrollBarChanged", horizontalScrollBar)
            searchBar.__horizontalScrollBar.value = horizontalScrollBar
        }

    }

    PsSearchBar {
        id: searchBar
        visible: false
        height: 30
        width: parent.width
        horizontalScrollBarPolicy: Qt.ScrollBarAlwaysOff
        anchors.bottom: parent.bottom        
    }

    SearchDialog {
        id: searchDialog
        Component.onCompleted: {
            searchDialog.okAndClose.connect(searchOkAndClose)
            tableView.clearSearch.connect(searchDialog.clearFields)
        }
        onVisibilityChanged: {
            if(!visible)
                tableView.forceActiveFocus()
        }
    }
    AddPaymentDialog {
        id: addPaymentDialog
        onVisibilityChanged: {
            if(!visible) {
                tableView.forceActiveFocus()
            } else {
                packField.text = paymentPackInfo.packName.text
            }
        }
    }
    PaymentPackDialog {
        id: paymentPackDialog
        visible: false
        onVisibilityChanged: {
            if(!visible)
                tableView.forceActiveFocus()
        }
        onAccepted: {
            paymentPackInfo.packName.text = packName.text
            paymentPackInfo.totalPayNumber.text = payNumber.text
            paymentPackInfo.totalPaySum.text = paySum.text
            statusWrite()
        }
        onRejected: {
            packName.text = ""
            payNumber.text = ""
            paySum.text = ""
    //        console.log("onRejected")
        }
    }
    PaymentPackInfo {
        id: paymentPackInfo
        visible: !paymentPackDialog.visible
//        width: 200
//        height: 200
    }

    MessageDialog {
        id: messageDialog
        //title: qsTr("Абонент отключен")
    }

    Timer {
        id: timerStatus
        interval: 10
        running: false
        repeat: false
        onTriggered: statusWrite()
    }

    onVisibleChanged: {
        if(visible){
            timerStatus.start()
        } else {
            statusClear()
        }
    }

    onSearchOkAndClose: {
        if(tableView.rowCount == 1){
            addPaymenAction.trigger()
        }
    }

    Action {
        id: searchBarAction
        enabled: tableView.visible
        text: !searchBar.visible?qsTr("Фильтр"):qsTr("Убрать фильтр")
        shortcut: "ctrl+f"
        tooltip: qsTr("Показать/Спрятать панель фильтра")
        onTriggered: {
            searchBar.listModel.clear();
            if (!searchBar.visible) {
                var columns = tableView.columnList()
                for (var i=0;i<columns.length;++i){
                    searchBar.listModel.append({"elementWidth": columns[i]["width"],
                                                   "elementRole": columns[i]["role"],
                                                   "elementTitle": columns[i]["title"]})
                }
                searchBar.visible = true;
                searchBar.listView.forceActiveFocus()
            } else {
                tableModel.clearFilter();
                tableModel.setFilterRoles();
                tableModel.setFullQuery();
                searchBar.visible = false
                tableView.forceActiveFocus()
            }
        }
    }

    Action {
        id: quickSearchAction
        enabled: tableView.visible
        text: qsTr("Быстрый поиск")
        shortcut: "ins"
        tooltip: qsTr("Открыть диалог поиска")
        onTriggered: {
            searchDialog.title = text
            searchDialog.visible = searchDialog.visible?false:true
        }
    }

    Action {
        id: addPaymenAction
        enabled: (rootWindow.userPermitPayment.indexOf("1")+1) && tableView.visible && ((tableView.rowCount==1?true:false)||
                                       (tableView.activeFocus && tableView.rowCount>1 && tableView.currentRow+1))
        text: qsTr("Добавить платеж")
        shortcut: "+"
        tooltip: qsTr("Открыть диалог добавления платежа")
        onTriggered: {
            //            tableView.selection.forEach( function(rowIndex) {console.log(rowIndex)} )
            //            console.log(tableView.currentRow)
            //            console.log(tableView.rowCount)//, tableModel.)
            var rowList = tableModel.rowToList(tableView.rowCount==1 ? 0 : tableView.currentRow)
            addPaymentDialog.uid = rowList[1][rowList[0].indexOf("uid")]
            addPaymentDialog.fio = rowList[1][rowList[0].indexOf("fio")]
            addPaymentDialog.address = rowList[1][rowList[0].indexOf("city")] + "," +
                    rowList[1][rowList[0].indexOf("address_street")] + "," +
                    rowList[1][rowList[0].indexOf("address_build")] + "," +
                    rowList[1][rowList[0].indexOf("address_flat")]
            addPaymentDialog.dogovor = rowList[1][rowList[0].indexOf("contract_id")]
            addPaymentDialog.dateDogovor = rowList[1][rowList[0].indexOf("contract_date")]
            //            console.log(addPaymentDialog.uid, addPaymentDialog.fio, addPaymentDialog.address)
            addPaymentDialog.title = text
            addPaymentDialog.visible = addPaymentDialog.visible ? false : true

        }
    }
    Action {
        id: retryPaymentAction
        enabled: tableView.visible && addPaymentDialog.uid + 1
        text: qsTr("Повторить платеж")
        shortcut: "/"
        tooltip: qsTr("Открыть диалог добавления платежа для последнего абонента")
        onTriggered: {
            //            addPaymentDialog.title = text
            addPaymentDialog.visible = addPaymentDialog.visible?false:true

        }
    }
    Action {
        id: clearSearchAction
        enabled: tableView.visible
        text: qsTr("Очистить параметры поиска")
        shortcut: "-"
        tooltip: qsTr("Отменить параметры поиска")
        onTriggered: {
            tableView.clearSearch()

        }
    }
    Action {
        id: paymentPackDialogAction
        enabled: tableView.visible
        text: qsTr("Параметры ввода пачки")
        shortcut: "*"
        tooltip: qsTr("Открыть параметры ввода пачки")
        onTriggered: {
            paymentPackDialog.open()
        }
    }

    MenuItem {
        id: quickSearchMenuItem
        action: quickSearchAction
    }
    MenuItem {
        id: searchBarMenuItem
        action: searchBarAction
    }
    MenuItem {
        id: addPaymenMenuItem
        action: addPaymenAction
    }
    MenuItem {
        id: retryPaymentMenuItem
        action: retryPaymentAction
    }
    MenuItem {
        id: clearSearchMenuItem
        action: clearSearchAction
    }
    MenuItem {
        id: paymentPackDialogItem
        action: paymentPackDialogAction
    }

    Component.onCompleted: {
        //        rootWindow.idMenuBar.manageMenu.insertItem(1,quickSearchMenu)
        rootWindow.idMenuBar.payMenu.visible = true        
        rootWindow.idMenuBar.payMenu.insertItem(0,addPaymenMenuItem)
        rootWindow.idMenuBar.payMenu.insertItem(1,retryPaymentMenuItem)
        rootWindow.idMenuBar.payMenu.insertItem(2,quickSearchMenuItem)
        rootWindow.idMenuBar.payMenu.insertItem(3,clearSearchMenuItem)
        rootWindow.idMenuBar.payMenu.insertItem(4,searchBarMenuItem)
        rootWindow.idMenuBar.payMenu.insertItem(5,paymentPackDialogItem)
        addPaymentDialog.refreshPayment.connect(timerStatus.start)
        addPaymentDialog.paymentAdded.connect(verifyPayment)
        timerStatus.start()
        paymentPackDialog.open()

    }

    Component.onDestruction: {
        rootWindow.idMenuBar.payMenu.visible = false        
        rootWindow.idMenuBar.payMenu.removeItem(quickSearchMenuItem)
        rootWindow.idMenuBar.payMenu.removeItem(searchBarMenuItem)
        rootWindow.idMenuBar.payMenu.removeItem(addPaymenMenuItem)
        rootWindow.idMenuBar.payMenu.removeItem(retryPaymentMenuItem)
        rootWindow.idMenuBar.payMenu.removeItem(clearSearchMenuItem)
        rootWindow.idMenuBar.payMenu.removeItem(paymentPackDialogItem)
        statusClear()
        console.log("Payment destruction")
    }

    function packCalc(userId, beginDate, endDate, isDateReg, pack)
    {
        userId = userId || rootWindow.userId
        if(beginDate === undefined){
            beginDate = new Date()
            beginDate.setHours(0,0,0,0)
        }
        if(endDate === undefined){
            endDate = new Date()
            endDate.setHours(23,59,59,999)
        }
        isDateReg = isDateReg || true
        pack = pack || paymentPackInfo.packName.text || addPaymentDialog.packField
        var packStr
        if(pack !== rootWindow.allValues){
            packStr = " AND inner_describe LIKE '%" + rootWindow.packAlias + pack + "'"
        } else {
            packStr = " "
        }
        var query = "SELECT COUNT(*), IFNULL(SUM(sum),0) FROM `payments` WHERE" +
                     (userId>=0?" aid=" + userId + " AND ":" ") +
                     (isDateReg?"reg_date":"date") + " BETWEEN '" +
                                beginDate.toLocaleString(Qt.locale(), "yyyy-MM-dd HH:mm:ss") + "' AND '" +
                                endDate.toLocaleString(Qt.locale(), "yyyy-MM-dd HH:mm:ss") + "'" +
                     packStr        
        return dBases.variantListFromDb(query)
    }

    function abonentCount()
    {        
        var whereStatement = tableModel.fullQuery(false, true, true, false)
        var query = tableModel.fullQuery(false) +
                (whereStatement.length>0?" AND disable=1":" WHERE disable=1")
        return dBases.listFromDb(query)
    }    

    function statusWrite()
    {
        var info = packCalc()
        rootWindow.statusLabel10.text = qsTr("  Пачка: ") + (paymentPackInfo.packName.text || addPaymentDialog.packField.text) +
                qsTr("  Платежей: ") + (Number(info[0][0]).toLocaleString(Qt.locale(), "f", 0) || 0) +
                qsTr("  На сумму: ") + (Number(info[0][1]).toLocaleString(Qt.locale()) || 0)

        if (paymentPackInfo.packName.text !== "") {
            paymentPackInfo.payNumber.text = Number(info[0][0]).toLocaleString(Qt.locale(), "f", 0) || 0
            paymentPackInfo.paySum.text = Number(info[0][1]).toLocaleString(Qt.locale()) || 0
        }

        info = abonentCount()
        rootWindow.statusLabel5.text = qsTr(" Абонентов: ") + Number(tableView.rowCount).toLocaleString(Qt.locale(),"f",0) +
                qsTr("  Отключено: ") + Number(info.length).toLocaleString(Qt.locale(),"f",0)
    }

    function statusClear()
    {        
        rootWindow.statusLabel5.text = ""
        rootWindow.statusLabel10.text = ""
//        rootWindow.statusLabel9.text = ""
    }

    function verifyPayment()
    {
        if (addPaymentDialog.uid > -1 && addPaymentDialog.paymentSum > 0) {
            var query = "SELECT `bills`.`deposit`, (`tarif_plans`.`month_fee` - `tarif_plans`.`month_fee` * `users`.`reduction` / 100),
                             `users`.`disable`, `users`.`deleted`
                         FROM `bills`, `users`, `tarif_plans`, `dv_main`
                         WHERE `users`.`uid` = " + addPaymentDialog.uid +
                         " AND `users`.`bill_id` = `bills`.`id`
                         AND `dv_main`.`uid` = `users`.`uid`
                         AND `tarif_plans`.`id` = `dv_main`.`tp_id`;"
            query = dBases.variantListFromDb(query)

            if (query[0][2] === "1" || query[0][3] === "1") {
                messageDialog.title = qsTr("Абонент отключен")
                messageDialog.text = qsTr("Внесена оплата отключенному абоненту")
                messageDialog.icon = StandardIcon.Warning
                messageDialog.visible = true
            } else if (query[0][0] > 2 * query[0][1]) {
                messageDialog.title = qsTr("Переплата")
                messageDialog.text = qsTr("У абонента переплата более двух месяцев!")
                messageDialog.icon = StandardIcon.Information
                messageDialog.visible = true
            } else if (query[0][0] <= 0 - query[0][1]) {
                messageDialog.title = qsTr("Задолженность")
                messageDialog.text = qsTr("После оплаты у абонента остается задолженность!")
                messageDialog.icon = StandardIcon.Information
                messageDialog.visible = true
            }
        }
    }
}
