import QtQuick 2.4
import QtQuick.Controls 1.3
import PsSqlTableModel 0.5

Item {
    id: itemTestView
    objectName: "objTabTestView"
    property int margins: 5
    property int fontSizeTop: 12
    property int fontSize1: 11
    property int fontSize2: 10

    PsTableView {
        id: tableView
        anchors.fill: parent
        sortIndicatorVisible: true
//        TableViewColumn { role: "uid"; title: "Login"; width: 100 }
//        TableViewColumn { role: "fio"; title: "FIO"; width: 200 }
        model: PsSqlTableModel {
            id: tableModel
            table: "users_pi"
            //sortColumn: tableView.sortIndicatorColumn||0
        }
        Component.onCompleted: {
            tableModel.setRelationTable(0, "users", "uid", "id");
            tableModel.generateRoleNames();
            tableModel.select();
            var list = tableModel.columnList()
            for (var i=0; i<list.length; ++i) {
                tableView.psAddColumn(list[i], list[i])
            }
        }
        onSortIndicatorColumnChanged: {
            tableModel.sortColumn = tableView.sortIndicatorColumn;
            tableModel.select();
        }
    }

}
