#ifndef QDBASES_H
#define QDBASES_H

#include <QObject>
#include <QtSql>
#include <QDebug>

class QDBases : public QObject
{
    Q_OBJECT
    Q_PROPERTY (QString hostName READ hostName WRITE setHostName NOTIFY hostNameChanged)
    Q_PROPERTY (QString dbName READ dbName WRITE setDbName NOTIFY dbNameChanged)
    Q_PROPERTY (QString userName READ userName WRITE setUserName NOTIFY userNameChanged)
    Q_PROPERTY (QString userPassword READ userPassword WRITE setUserPassword NOTIFY userPasswordChanged)
    Q_PROPERTY (int dbPort READ dbPort WRITE setDbPort NOTIFY dbPortChanged)
    Q_PROPERTY (QString dbType READ dbType WRITE setDbType NOTIFY dbTypeChanged)
public:
    explicit QDBases(QObject *parent = 0);
    ~QDBases();    
    Q_INVOKABLE bool connectToDb();
    Q_INVOKABLE void closeConnection();
    Q_INVOKABLE QStringList listFromDb(QString str, int columnCount = 1, QString delimeter = " ");
    Q_INVOKABLE QVariantList variantListFromDb(QString str);
    QString hostName() const;
    QString dbName() const;
    QString userName() const;
    QString userPassword() const;
    int dbPort() const;
    QString dbType() const;
    void setHostName(const QString& host);
    void setDbName(const QString& dbname);
    void setUserName(const QString& user);
    void setUserPassword(const QString& pass);
    Q_INVOKABLE void setDbPort(const int& port=3306);
    Q_INVOKABLE void setDbType(const QString& dbtype="QMYSQL");

signals:    
    void hostNameChanged();
    void dbNameChanged();
    void userNameChanged();
    void userPasswordChanged();
    void dbPortChanged();
    void dbTypeChanged();

public slots:    

private:
     QSqlDatabase db;
     QString m_hostName;
     QString m_dbName;
     QString m_userName;
     QString m_userPassword;
     int m_dbPort;
     QString m_dbType;
};

#endif // QDBASES_H
