import QtQuick 2.7
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3

Window {
    id: packInfoWindow
    title: qsTr("Пачка")
    flags: Qt.Tool
    property alias packName: packName
    property alias payNumber: payNumber
    property alias paySum: paySum
    property alias totalPayNumber: totalPayNumber
    property alias totalPaySum: totalPaySum

    GridLayout {
        id: layout1
        columns: 2
        anchors {
            margins: rootWindow.margins
            top: parent.top
            left: parent.left
            right: parent.right
//            fill: parent
        }
        columnSpacing: rootWindow.margins
        rowSpacing: rootWindow.margins
        Label {
            text: qsTr("Пачка:")
        }
        Label {
            id: packName
            font.bold: true
        }
        Label {
            text: qsTr("Введено платежей:")
        }
        Label {
            id: payNumber
            font.bold: true
            onTextChanged: {
               totalPay()
            }
        }
        Label {
            text: qsTr("Сумма:")
        }
        Label {
            id: paySum
            font.bold: true
            onTextChanged: {
                totalPay()
            }
        }
        Label {
            text: qsTr("Общее количество:")
        }
        Label {
            id: totalPayNumber
            font.bold: true
        }
        Label {
            text: qsTr("Общая сумма:")
        }
        Label {
            id: totalPaySum
            font.bold: true
        }        
    }
//    RowLayout {
//        id: layout2
//        anchors {
//            margins: rootWindow.margins
//            top: layout1.bottom
////            right: parent.right
////            left: parent.left
//            horizontalCenter: parent.horizontalCenter
//        }
//        spacing: rootWindow.margins
//        Button {
//            id: applyButton
//            text: qsTr("Применить")
//            enabled: false
//            onClicked: {
//                enabled = false
//                packInfoWindow.close()
//            }
//        }
//        Button {
//            id: cancelButton
//            text: qsTr("Отмена")
//            onClicked: {
//                packInfoWindow.close()
//            }
//        }
//    }

    function totalPay()
    {
        if ((payNumber.text !== totalPayNumber.text) || (paySum.text !== totalPaySum.text)) {
            paySum.color = "red"
            payNumber.color = "red"
//            applyButton.enabled = false
        } else {
            paySum.color = "green"
            payNumber.color = "green"
//            applyButton.enabled = true
        }
    }
}
