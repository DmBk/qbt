TEMPLATE = app

QT += qml quick sql

SOURCES += main.cpp \
    qdbases.cpp \
    pssqltablemodel.cpp \
    psuiproxy.cpp \
    pssqlquerymodel.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    qdbases.h \
    pssqltablemodel.h \
    psuiproxy.h \
    pssqlquerymodel.h

DISTFILES +=
