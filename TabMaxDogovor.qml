import QtQuick 2.3

Item {
    id: itemMaxDogovor
    Label {
        id: idLabel
        text: qsTr("Последние договора")
        font.pointSize: 10
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.margins: 5
    }
    TextArea {
        id: idText
        font.pointSize: 10
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: idLabel.bottom
        anchors.margins: 5
    }
    Button {
        text: qsTr("Обновить")
        anchors.left: idLabel.right
        anchors.top: parent.top
        onClicked: parent.orderDogovor()
    }

    Component.onCompleted: {
        orderDogovor();
    }

    function orderDogovor ()
    {
        var query = "SELECT contract_id, fio, CONCAT(city, ',', address_street, ',', address_build, ',', address_flat) as address
                      FROM `users_pi`
                      ORDER BY  CONVERT(contract_id, UNSIGNED) DESC
                      LIMIT 0, 20";
        query = dBases.variantListFromDb(query);
        for (var i=0; i<query.length; ++i) {
            idText.append(query[i].[0] + " " + query[i].[1] + " " + query[i].[2] + " ");
        }
    }
}

