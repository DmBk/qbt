import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import QtQuick.Controls.Styles 1.4

//Item {
//    property int calendarX
//    property int calendarY
//    property alias calendarVisible: calendarButton.checked
//    property alias displayText: calendarText.displayText
//    property alias selectedDate: calendar.selectedDate
//    width: 96
TextField {
    id: calendarText
        property int calendarX
        property int calendarY
        property alias calendarVisible: calendarButton.checked
    inputMask: "9999-99-99;0"
    //    text: new Date()
    text: calendar.selectedDate
    horizontalAlignment: Qt.AlignLeft
//    width: parent.width - height
//}

    Button {
        id: calendarButton
        checkable: true
        width: calendarText.height
        height: calendarText.height
        activeFocusOnTab: false
        iconSource: calendarDialog.visible?"images/calendar-close.png":"images/calendar.png"
        anchors.right: calendarText.right
    }

    Window {
        id: calendarDialog
        flags: Qt.Popup //Qt.SplashScreen
        width: calendar.width
        height: calendar.height
        visible: calendarButton.checked && calendarText.visible
//        x: calendarX - calendar.width
//        y: calendarY
        x: calendarX + calendarText.width - calendar.width
        y: calendarY + calendarText.height

        Calendar {
            id: calendar
            implicitWidth: 210
            implicitHeight: implicitWidth
            onClicked: {
                calendarButton.checked = false
                //                calendarText.text = calendar.selectedDate
            }
        }
    }
    Component.onDestruction: {
        calendarDialog.destroy()
    }
}
