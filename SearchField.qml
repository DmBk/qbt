import QtQuick 2.5
import QtQuick.Controls 1.4

TextField {
    signal fieldTextChanged(string role, string value)
    id: searchField
    onTextChanged: {
        searchField.fieldTextChanged(searchField.objectName, searchField.text)
//        console.log(searchField.objectName, searchField.getText(0,cursorPosition))
    }
}

