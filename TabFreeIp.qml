import QtQuick 2.5
import QtQuick.Controls 1.4

Item {
    id: itemFreeIp
//    property var historyModel: ListModel{}

    Label {
        id: idLabelTop
        text: qsTr("Диапазон IP адресов")
        font.pointSize: 9
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.margins: rootWindow.margins
    }
    IpAddrField {
        id: beginIpRangeField        
        focus: true
//        text: "176.121.242.2"
        anchors.top: idLabelTop.bottom
        anchors.right: parent.horizontalCenter
        anchors.margins: rootWindow.margins
        onDisplayTextChanged: {
            if ((beginIpRangeField.cursorPosition <= 12) ) {
                parent.beginIpRangeChanged(beginIpRangeField.cursorPosition);
            }
        }        
    }
    IpAddrField {
        id: endIpRangeField
//        text: "176.121.242.254"
        anchors.top: idLabelTop.bottom
        anchors.left: parent.horizontalCenter
        anchors.margins: rootWindow.margins
    }

    Button {
//        text: qsTr("Найти")
        anchors.top: idLabelTop.bottom
        anchors.left: endIpRangeField.right
        anchors.margins: rootWindow.margins
        focus: true
        Keys.onReturnPressed: refreshAction.trigger()
        Keys.onEnterPressed: refreshAction.trigger()
        action: refreshAction
    }
    Label {
        id: idLabel2
        text: qsTr("Свободные IP адреса")
        font.pointSize: 9
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: beginIpRangeField.bottom
        anchors.margins: rootWindow.margins
    }
    TextArea {
        id: idText
        horizontalAlignment:  Qt.AlignJustify // Text.AlignJustify
        readOnly: true
        font.pointSize: 11        
        anchors.top: idLabel2.bottom
        anchors.left: beginIpRangeField.left
        anchors.right: endIpRangeField.right
        backgroundVisible: false
    }    
    Label {
        id: idLabel3
        text: qsTr("Сохранять историю")
        verticalAlignment: Text.AlignVCenter
        anchors {
            margins: rootWindow.margins
            bottomMargin: 0
            top: historyLineCount.top
            bottom: historyTable.top
            left: idText.right
        }
    }
    SpinBox {
        id: historyLineCount
        suffix: qsTr(" запросов")
        value: 10.0
        anchors {
            bottomMargin: 0
            bottom: historyTable.top
            left: idLabel3.right
        }
    }
    TableView {
        id: historyTable
        property var arrHistory: []
        headerVisible: false
        anchors {
            margins: rootWindow.margins
            topMargin: 0
            rightMargin: 0
            top: idText.top
            left: idText.right
            right: historyLineCount.right
        }

        TableViewColumn {}
        model: arrHistory

        onClicked: {
            var arrIp = arrHistory[row].split("-");
            beginIpRangeField.text = arrIp[0] || "0.0.0.0";
            endIpRangeField.text = arrIp[1] || "0.0.0.0";
            refreshAction.trigger();
        }

        onRowCountChanged: {
            height = Math.min(Math.max(__listView.contentHeight + 2, historyTable.height),
                              (itemFreeIp.height - idLabelTop.height - beginIpRangeField.height - idLabel2.height - 24));
        }
    }

    signal searchIp ()

    onSearchIp: {        
        var i = ipToInt(beginIpRangeField.text);
        var j = ipToInt(endIpRangeField.text);                
        idText.text = "";
        if ((i>=0) && (j>i)) {
            //            var dt1 = new Date();
            var queryStr = "SELECT INET_ATON(users_pi._ip_adr) FROM users_pi
                            WHERE INET_ATON(users_pi._ip_adr) >= " + i +
" AND INET_ATON(users_pi._ip_adr) <= " + j + " ORDER BY INET_ATON(users_pi._ip_adr);";
            var listIp = dBases.listFromDb(queryStr);
            //            var dt2 = new Date();
            queryStr = "SELECT COUNT(users_pi._ip_adr) FROM users_pi
                        WHERE INET_ATON(users_pi._ip_adr) >= " + i + " AND INET_ATON(users_pi._ip_adr) <= " + j + ";";
            var countIp = dBases.listFromDb(queryStr);
            var totalIp = j -i + 1;
            queryStr = "SELECT _ip_adr, COUNT(_ip_adr) FROM users_pi
                        WHERE INET_ATON(_ip_adr) >= " + i + " AND INET_ATON(_ip_adr) <= " +
j + " GROUP BY _ip_adr HAVING COUNT(_ip_adr)>1
                        ORDER BY COUNT(_ip_adr)DESC;";
            var doubleIp = dBases.variantListFromDb(queryStr);
            queryStr = "SELECT up._ip_adr, aa.datetime
                        FROM `admin_actions` aa, `users_pi` up, `users` u WHERE
                        u.uid=up.uid AND
                        aa.uid=u.uid AND
                        (u.disable=1 OR u.deleted = 1) AND
                        aa.id = (SELECT MAX(aa2.id)
                                 FROM `admin_actions` aa2 WHERE aa2.uid=u.uid AND aa2.action_type=9 ) AND
                        INET_ATON(up._ip_adr) >= " + i + " AND INET_ATON(up._ip_adr) <= " + j +" ORDER BY aa.datetime;";
            var disableIp = dBases.variantListFromDb(queryStr);
            //            var dt3 = new Date();
            idText.append(qsTr("Диапазон из %1 IP адресов").arg(totalIp));
            idText.append(qsTr("Количество IP адресов в базе: %1").arg(countIp[0]));
            for (var k=0; k<doubleIp.length; ++k){
                idText.append(qsTr("Двойное использование IP адресов в базе: %1 - %2").
                              arg(doubleIp[k][0]).arg(doubleIp[k][1]));
            }
            idText.append(qsTr("Свободные IP адреса:"));
            for (var ii=0; i<=j; ++i){
                if ((ii=listIp.indexOf(i.toString()))>=0) {
                    listIp = listIp.slice(ii+1);
                } else {
                    idText.append(intToIp(i));
                }
            }
            //            var dt4 = new Date();
            idText.append(qsTr("Отключены %1:").arg(disableIp.length));
            var locale = Qt.locale();
            var length = disableIp.length;
            for (var k=0; k<length; ++k){
                idText.append(alignIp(disableIp[k][0]) + " ");
                idText.insert(idText.cursorPosition, Date.fromLocaleString(locale, disableIp[k][1], "yyyy-MM-ddThh:mm:ss").
                              toLocaleString(Locale.ShortFormat));
            }
            //            var dt5 = new Date();
            //            idText.append("dt2=" + (dt2.getTime() - dt1.getTime()));
            //            idText.append("dt3=" + (dt3.getTime() - dt1.getTime()));
            //            idText.append("dt4=" + (dt4.getTime() - dt1.getTime()));
            //            idText.append("dt5=" + (dt5.getTime() - dt1.getTime()));
            idText.cursorPosition = 0;
            itemFreeIp.heightChanged();
        } else {
            idText.append(qsTr("Неправильный диапазон"));
        }

        writeParam();
        readParam();
    }

    function ipToInt (ip)
    {
        var part = ip.split('.');
        if ((part[0]<256) && (part[1]<256) && (part[2]<256) && (part[3]<256)) {
            return ((((((+part[0])*256)+(+part[1]))*256)+(+part[2]))*256)+(+part[3]);
        }
        return "Error";
    }

    function intToIp(num)
    {
        var ip = num%256;
        for (var i = 3; i > 0; --i)
        {
            num = Math.floor(num/256);
            ip = num%256 + '.' + ip;
        }

        return ip;
    }

    function alignIp (ip)
    {
        while (ip.length < 15) {
            ip += " ";
        }
        return ip;
    }

    function readParam()
    {
        var history = rootWindow.configDb.readParam("free_ip", "%", "ORDER BY param");
        historyTable.arrHistory.length = 0;
        for(var i in history){            
            if(i != "historyLine"){
                historyTable.arrHistory.push(i);
            } else {
                historyLineCount.value = history[i];
            }
        }
        historyTable.modelChanged();
    }

    function writeParam(){
        var datetime = new Date();
        rootWindow.configDb.writeParam("free_ip", "historyLine", historyLineCount.value);
        rootWindow.configDb.writeParam("free_ip", beginIpRangeField.text + "-" + endIpRangeField.text, datetime);
        //при уменьшении строк истории удалить лишние
        while(historyTable.arrHistory.length > historyLineCount.value){            
            deleteOldestRange();
        }
        //если поиск вызван из historyTable ничего удалять не нужно
        if((historyTable.arrHistory.length == historyLineCount.value) &&
           (historyTable.arrHistory.indexOf(beginIpRangeField.text + "-" + endIpRangeField.text) < 0)){
            deleteOldestRange();
        }
    }

    function deleteOldestRange() {
        var arrRange = rootWindow.configDb.readParam("free_ip", "%", " ORDER BY value LIMIT 0,2");
        var ipRange;
        for (var i in arrRange) {
            if(i != "historyLine") {
                ipRange = i;
                break;
            }
        }
        var index = historyTable.arrHistory.indexOf(ipRange);
        historyTable.arrHistory.splice(index, 1);
        rootWindow.configDb.deleteParam("free_ip", ipRange);
        historyTable.modelChanged();
    }

    onHeightChanged: {
        idText.height = Math.min(Math.max(idText.contentHeight + 2, idText.height),
                                 (itemFreeIp.height - idLabelTop.height - beginIpRangeField.height - idLabel2.height - 24));
    }

    function beginIpRangeChanged(position)
    {
        endIpRangeField.insert(0, beginIpRangeField.getText(0, position+1 ));
    }

    Action {
        id: refreshAction
        text: qsTr("Найти")
        enabled: idLabelTop.visible
        onTriggered: searchIp()
        shortcut: "F5"
        tooltip: qsTr("Найти IP")
    }

    Component.onCompleted: {
        beginIpRangeField.forceActiveFocus();
        beginIpRangeField.cursorPosition = 0;
        itemFreeIp.readParam();
    }    
}
