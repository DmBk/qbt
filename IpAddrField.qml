import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

TextField {
    id: ipField
    font.pointSize: 11
    implicitWidth: font.pixelSize/1.6 * maximumLength
    //                implicitHeight: font.pixelSize <= 20? font.pixelSize * 2:font.pixelSize * 1.4
    maximumLength: 15
    horizontalAlignment: Text.AlignHCenter
    inputMask: "000.000.000.000;_"
    validator: IntValidator {bottom: 0; top: 255;}
    style: TextFieldStyle {
        id: idTextFieldStyle
        textColor: "black"
        background: Rectangle {
            radius: 2
            //          implicitWidth: 100
            //          implicitHeight: 24
            //            color: "white"
            //            border.color: "#ffffff"
            border.width: 1
        }
    }    
}
