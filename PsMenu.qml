import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
Item{
    property alias idMenuBar: idMenuBar
    //    property alias manageMenu: manageMenu

    signal tabRenameSwitch()

    MenuBar {
        id: idMenuBar
        property alias payMenu: payMenu
        property alias paymentsMenu: paymentsMenu
        Menu {
            title: qsTr("Файл")
            MenuItem {
                action: loginAction
            }
            MenuItem {
                action: exitAction
            }
        }
        Menu {
            title: qsTr("Задачи")
            MenuItem {
                action: freeIpAction
            }
            MenuItem {
                action: maxContractAction
            }
            MenuItem {
                action: renameSwAction
            }
            MenuItem {
                action: paymentAction
            }
            MenuItem {
                action: paymentsAction
            }
        }

        Menu {
            id: payMenu
            title: qsTr("Оплата")
            visible: false
        }

        Menu {
            id: paymentsMenu
            title: qsTr("Платежи")
            visible: false
        }

        Menu {
            title: qsTr("Настройки")
            MenuItem {
                action: settingsAction
            }
        }

        Menu {
            title: qsTr("Помощь")
            MenuItem {
                action: aboutAction
            }
        }
    }

    Action {
        id: exitAction
        text: qsTr("Выход")
        onTriggered: Qt.quit();
        shortcut: StandardKey.Quit
        tooltip: qsTr("Выход из программы")
    }
    Action {
        id: renameSwAction
        enabled: rootWindow.connectSuccessful
        text: qsTr("Замена свича")
        shortcut: "ctrl+shift+r"
        tooltip: qsTr("Переименование свича в базе")
        onTriggered: {
            var component = Qt.createComponent("TabRenameSwitch.qml")
            if (component.status === Component.Ready) {
                var newTab = tabView.addTab(qsTr("Замена свича"), component);
                console.log("Component.Ready")
                tabView.currentIndex = tabView.count - 1
            } else {
                console.log("Component.NotReady")
            }
        }
    }
    Action {
        id: freeIpAction
        enabled: rootWindow.connectSuccessful
        text: qsTr("Свободные IP")
        shortcut: "ctrl+shift+i"
        tooltip: qsTr("Поиск не занятых IP адресов в базе")
        onTriggered: {
            var component = Qt.createComponent("TabFreeIp.qml")
            if (component.status === Component.Ready) {
                //                var newTab = component.createObject(tabView)
                var newTab = tabView.addTab(qsTr("Свободные IP"), component);
                console.log("Component.Ready")
                console.log(newTab.objectName)
                tabView.currentIndex = tabView.count -1
            } else {
                console.log("Component.NotReady")
            }
        }
    }
    Action {
        id: maxContractAction
        enabled: rootWindow.connectSuccessful
        text: qsTr("Последний договор")
        shortcut: "ctrl+shift+d"
        tooltip: qsTr("Поиск последнего номера догора в базе")
        onTriggered: {
            if(!activateTab(qsTr("Последний договор"))){
                var component = Qt.createComponent("TabMaxContract.qml")
                if (component.status === Component.Ready) {
                    var newTab = tabView.addTab(qsTr("Последний договор"), component);
                    console.log("Component.Ready")
                    //                console.log(newTab.objectName)
                    tabView.currentIndex = tabView.count -1
                } else {
                    console.log("Component.NotReady")
                }
            }
        }
    }

    Action {
        id: paymentAction
        enabled: rootWindow.connectSuccessful
        text: qsTr("Внести оплату")
        shortcut: "ctrl+shift+P"
        tooltip: qsTr("Внести оплату")
        onTriggered: {
            if(!activateTab(qsTr("Оплата"))){
                var component = Qt.createComponent("TabPayment.qml")
                if (component.status === Component.Ready) {
                    var newTab = tabView.addTab(qsTr("Оплата"), component);
                    console.log("Component.Ready")
                    tabView.currentIndex = tabView.count -1
                } else {
                    console.log("Component.NotReady")
                }
            }
        }
    }

    Action {
        id: paymentsAction
        enabled: rootWindow.connectSuccessful && (rootWindow.userPermitPayment!=undefined?rootWindow.userPermitPayment.indexOf("0")+1:false)
        text: qsTr("Список платежей")
        shortcut: "Ctrl+Shift+O"
        tooltip: qsTr("Список платежей")
        onTriggered: {
            if(!activateTab(qsTr("Платежи"))){
                var component = Qt.createComponent("TabPayments.qml")
                if (component.status === Component.Ready) {
                    var newTab = tabView.addTab(qsTr("Платежи"), component);
                    console.log("Component.Ready")
                    tabView.currentIndex = tabView.count -1
                } else {
                    console.log("Component.NotReady")
                }
            }
        }
    }

    Action {
        id: settingsAction
        //        enabled: !rootWindow.tabOpen["settings"]
        text: qsTr("Настройки")
        shortcut: "Ctrl+Shift+S"
        tooltip: qsTr("Настройки программы")
        onTriggered: {
            if(!activateTab(settingsAction.text)){
                var component = Qt.createComponent("TabSettings.qml")
                if (component.status === Component.Ready) {
                    var newTab = tabView.addTab(settingsAction.text, component);
                    console.log("Component.Ready")
                    tabView.currentIndex = tabView.count -1
                } else {
                    console.log("Component.NotReady")
                }
            }
        }
    }

    Action {
        id: loginAction
        text: qsTr("Авторизация")
        tooltip: qsTr("Войти в систему")
        onTriggered: {
            rootWindow.loginDialog.visible = true
        }
    }

    Action {
        id: aboutAction
        text: qsTr("О программе")
        tooltip: qsTr("Информация о программе")
        onTriggered: {
            rootWindow.about.open()
        }
    }

    function activateTab(title)
    {
        var tab;
        for(var i=0;i<tabView.count;++i){
            tab = tabView.getTab(i);
            if(tab.title === title){
                tabView.currentIndex = i;
                return true;
            }
        }
        return false;
    }
}
