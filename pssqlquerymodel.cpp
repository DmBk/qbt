#include "pssqlquerymodel.h"

PsSqlQueryModel::PsSqlQueryModel(QObject *parent) : QSqlQueryModel(parent)
{ 
    p_db = new QSqlDatabase();
    m_strictFilter = false;
    QObject::connect(this, SIGNAL(selectStringChanged()), this, SLOT(generateRoleNames()));
}

PsSqlQueryModel::~PsSqlQueryModel()
{
    delete p_db;
}

void PsSqlQueryModel::generateRoleNames()
{    
    roles.clear();
    //    QSqlQuery query(m_select + filterRoles() + searchRoles());
    QSqlQuery query;
    if(!m_select.contains(" LIMIT ", Qt::CaseInsensitive)){
        query.exec(m_select + " LIMIT 0");
    } else {
        query.exec(m_select);
    }
    QSqlRecord rec = query.record();
    //    qDebug() << "columnCount=" << columnCount();
    //    qDebug() << "columnCount=" << rec.count();
    for (int i=0; i < rec.count(); ++i) {
        //        roles[Qt::UserRole + i + 1] = this->headerData(i, Qt::Horizontal).toByteArray();
        roles[Qt::UserRole + i + 1] = QVariant(rec.fieldName(i)).toByteArray();
        //        qDebug() << "roles[" << Qt::UserRole + i + 1 << "]=" << roles[Qt::UserRole + i + 1];
    }
}

QVariant PsSqlQueryModel::data(const QModelIndex &index, int role) const
{
    //    qDebug() << "PsSqlQueryModel::data role=" << role << "index.row()="<< index.row();
    if(index.row() >= rowCount()) {
        return QString("");
    }
    if(role < Qt::UserRole) {
        return QSqlQueryModel::data(index, role);
    } else {
        return QSqlQueryModel::data(this->index(index.row(), role - Qt::UserRole - 1), Qt::DisplayRole);
    }
}

void PsSqlQueryModel::setSelectString(const QString &select)
{
    //    beginResetModel();
    //    clear();
    if(m_select!=select){
        m_select = select;
        emit selectStringChanged();
    }
    clearMultiRole();

    //    m_where = where;
    //    m_order = order;
    //    QSqlQuery query(m_select, *p_db);
    //    setQuery(query);
    //    generateRoleNames();
    //    endResetModel();
    //    qDebug() << "endResetModel();";
}

QString PsSqlQueryModel::selectString() const
{
    return m_select;
}

void PsSqlQueryModel::setSortRoles(const QString &role, Qt::SortOrder order)
{
    m_order = role;

    if (order==Qt::DescendingOrder) {
        m_order += " DESC";
    }

    //    setFullQuery();
}

QString PsSqlQueryModel::sortRoles() const
{
    return m_order.isEmpty()?m_order:" ORDER BY " + m_order;
}

void PsSqlQueryModel::setFullQuery()
{
    beginResetModel();
    clear();
//    qDebug() << m_select + filterRoles() + searchRoles() + sortRoles();
    QSqlQuery query(m_select + filterRoles() + searchRoles() + sortRoles(), *p_db);
    setQuery(query);
    //    generateRoleNames();
    endResetModel();
}

QString PsSqlQueryModel::fullQuery(const bool widthSort, const bool widthSearch,
                                   const bool widthFilter, const bool widthSelect) const
{
    return (widthSelect?m_select:QString()) + (widthFilter?filterRoles():QString()) +
            (widthSearch?searchRoles():QString()) + (widthSort?sortRoles():QString());
}

QString PsSqlQueryModel::filterRoles() const
{
    QString filter;
    if(!h_filter.isEmpty()){
        QString strictValue = m_strictFilter?QString():"%";
        QHash<QString,QString>::const_iterator it=h_filter.begin();
        filter.append(" WHERE " + it.key() + " LIKE '" + strictValue + it.value() + strictValue + "' ");
        for(++it;it!=h_filter.end();++it){
            filter.append("AND " + it.key() + " LIKE '" + strictValue + it.value() + strictValue + "' ");
        }
    }
    if(m_strictFilter){
        filter.replace(QString("*"), QString("%"));
    }
    return filter;
}

void PsSqlQueryModel::setFilterRoles(QString role, const QString &value)
{
    if (!role.isEmpty()){
        if((!h_multiRole.isEmpty())&&(h_multiRole.contains(role))){
            role = h_multiRole[role] + "." + role;
        }
        if(!value.isEmpty()){
            h_filter.insert(role, value);
        } else {
            h_filter.remove(role);
        }
    }
    //    setFullQuery();
}

void PsSqlQueryModel::clearFilter()
{
    h_filter.clear();
}

QString PsSqlQueryModel::searchRoles() const
{
    QString search;
    if(!h_search.isEmpty()){
        QHash<QString,QString>::const_iterator it=h_search.begin();
        QString strictValue = m_strictFilter?QString():"%";
        if (h_filter.isEmpty()) {
            search.append(" WHERE " + it.key() + " LIKE '" + strictValue + it.value() + strictValue + "' ");
        } else {
            search.append(" AND " + it.key() + " LIKE '" + strictValue + it.value() + strictValue + "' ");
        }
        for(++it;it!=h_search.end();++it){
            search.append("AND " + it.key() + " LIKE '" + strictValue + it.value() + strictValue + "' ");
        }
    }
    if(!h_dateSearch.isEmpty()){
        QHash<QString,QString>::const_iterator itDate=h_dateSearch.begin();
        for(;itDate!=h_dateSearch.end();++itDate){
            if(h_filter.isEmpty()&&search.isEmpty()){
                search.append(" WHERE " + itDate.key() + " " + itDate.value());
            } else {
                search.append(" AND " + itDate.key() + " " + itDate.value());
            }
        }
    }
    if(m_strictFilter) {
        search.replace(QString("*"), QString("%"));
    }
    return search;
}

void PsSqlQueryModel::setSearchRoles(QString role, const QString &value, const bool &isDate)
{
    if (!role.isEmpty()){
        if((!h_multiRole.isEmpty())&&(h_multiRole.contains(role))){
            role = h_multiRole[role] + "." + role;
        }
        if(!value.isEmpty()){
            if(!isDate){
                h_search.insert(role, value);
            } else {
                h_dateSearch.insert(role, value);
            }
        } else {
            if(!isDate){
                h_search.remove(role);
            } else {
                h_dateSearch.remove(role);
            }

        }
    }
    //    setFullQuery();
}

void PsSqlQueryModel::clearSearch()
{
    h_search.clear();
}
QVariantList PsSqlQueryModel::rowToList(const int row) const
{
    QSqlRecord rec=this->record(row);
    QStringList listRole;
    QStringList listValue;
    for (int i=0; i<rec.count(); ++i){
        listRole.append(rec.fieldName(i));
        listValue.append(rec.value(i).toString());
    }
    return QVariantList() << listRole << listValue;
}

QVariant PsSqlQueryModel::itemFromRow(const int row, const QString role) const
{
    return this->record(row).value(role);
}

bool PsSqlQueryModel::strictFilter() const
{
    return m_strictFilter;
}
void PsSqlQueryModel::setStrictFilter(const bool strict)
{
    m_strictFilter = strict;
}

void PsSqlQueryModel::setMultiRole(const QString &role, const QString &mainTable)
{
    if (!role.isEmpty()){
        if(!mainTable.isEmpty()){
            h_multiRole.insert(role, mainTable);
        } else {
            h_multiRole.remove(role);
        }
    }
}

void PsSqlQueryModel::clearMultiRole()
{
    h_multiRole.clear();
}
