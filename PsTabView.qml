import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
TabView {    
    frameVisible: true
    id: idTabView
    property bool closingTab: true
    property bool nextTab: true
    property color tabColor: rootWindow.colorList.getColor("tabViewTab")||"lightsteelblue"
    property color selectedTabColor: rootWindow.colorList.getColor("tabViewSelectedTab")||"steelblue"
    property color backgroundColor: rootWindow.colorList.getColor("background")||"steelblue"
    signal closeButtonClicked(int tabIndex)
    onCloseButtonClicked: {
        removeTab(tabIndex)
//        console.log("tab removed")
//        var tab = getTab(tabIndex)
//        tab.destroy()
    }

    signal tabCreated (string objName)

    function psAddTab ()
    {
        var newTab = Qt.createQmlObject('import QtQuick 2.5; import QtQuick.Controls 1.4; Tab {}', idTabView, '');
        newTab.title = "title"
        newTab.objectName = "objName"
        var component = Qt.createQmlObject('import QtQuick 2.5; Rectangle {color: "red"}',newTab, '')
//        component.color = "red"
        component.anchors.fill = newTab
        //        var component = Qt.createComponent("TabRenameSwitch.qml")
        //        if (component.status == Component.Ready) {
        //            var rect = component.createObject(newTab)
        //        }
    }

    //    function psAddTab() {
    //        var newTab = Qt.createQmlObject('import QtQuick 2.5; import QtQuick.Controls 1.4; Tab {}', tabView, '');
    //        newTab.title = "852"
    //        newTab.objectName = "tab123"
    //        var component = Qt.createComponent("PsTableView.qml")
    //        if (component.status == Component.Ready) {
    //            var newModel = component.createObject(newTab, {model: tableModel, objectName: "myTableView"})
    //            newModel.anchors.fill = newTab;
    //            console.log("Component.Ready")
    //            console.log(newModel.objectName)
    //        } else {
    //            console.log("Component.NotReady")
    //        }

    //        newTab.active = true
    //        tabView.currentIndex = tabView.count - 1
    //        console.log(tabView.currentIndex)
    ////        rootWindow.newTabCreated(newModel.objectName)
    //        tabCreated(newModel.objectName)
    //        newModel.destroy(10000)
    //        newTab.destroy(10000)

    //    }

    //    function findChild(item, objectName) {
    //            const children = item.children;
    //            for (var i = children.length; --i >= 0; ) {
    //                var child = children[i];
    //                if (!child.objectName)
    //                    continue;
    //                if (child.objectName === objectName)
    //                    return child;
    //                child = findChild(child, objectName);
    //                if (child)
    //                    return child;
    //            }
    //            return null;
    //        }

    style: TabViewStyle {
        frameOverlap: 1
        tab: Rectangle {
            id: idTab
            color: styleData.selected ? selectedTabColor : tabColor
            border.color:  selectedTabColor
            //                implicitWidth: Math.max(idTextTitle.width + 4, 90)
            implicitWidth: 100
            implicitHeight: 20
            radius: 3
            Text {
                id: idTextTitle                
                text: styleData.title
                elide: Text.ElideRight
//                clip: true
                color: styleData.selected ? "white" : "black"
                width: idTab.implicitWidth - idCloseButton.implicitWidth/2
                horizontalAlignment: Qt.AlignHCenter
                anchors.centerIn: parent
//                anchors.left: idTab.left
//                anchors.right: idCloseButton.left
            }
            Button {
                id: idCloseButton
                text: "X"
                activeFocusOnTab: false
                visible: idTabView.closingTab
                implicitWidth: 11
                implicitHeight: implicitWidth
                anchors.right: parent.right
                anchors.top: parent.top
                onClicked: idTabView.closeButtonClicked(styleData.index)
                anchors.margins: 1
                style: ButtonStyle {
                    background: Rectangle {
                        id: rect
//                        border.width: control.activeFocus ? 2 : 1
//                        border.color: control.hovered?"IndianRed": idTab.color //"#888"
                        radius: 4
                        color: "IndianRed" //control.hovered?"IndianRed": idTab.color
                        opacity: control.hovered?1:0
                        //                                gradient: Gradient {
                        //                                    GradientStop { position: 0 ; color: control.pressed ? "#ccc" : "#eee" }
                        //                                    GradientStop { position: 1 ; color: control.pressed ? "#aaa" : "#ccc" }
                        //                                }
                    }
                    label: Component {
                        Text {
                            text: idCloseButton.text
//                            clip: true
//                            wrapMode: Text.WordWrap
                            color: control.hovered?"white": "black"
                            verticalAlignment: Text.AlignVCenter
                            horizontalAlignment: Text.AlignHCenter
                            font.pixelSize: idCloseButton.implicitWidth - 3
                            anchors.fill: parent
                        }
                    }
                }
            }
        }
//        frame: Rectangle { color: "steelblue" }
        frame: Rectangle { color: backgroundColor }

        //            rightCorner: Row {
        //                Rectangle{
        //                width: 18
        //                height: 18
        //                color: "red"
        //            }
        //            }
    }

    Action {
        id: nextTab
        enabled: visible && idTabView.nextTab && idTabView.count>1
        text: qsTr("Следующая вкладка")
        shortcut: "Ctrl+Tab"
        tooltip: qsTr("Перейти на следующую вкладку")
        onTriggered: {
            idTabView.currentIndex = (idTabView.currentIndex+1<idTabView.count?++idTabView.currentIndex:0)
        }
    }
    Action {
        id: closeTab
        enabled: visible && idTabView.closingTab && idTabView.count>0
        text: qsTr("Закрыть вкладку")
        shortcut: "Ctrl+W"
        tooltip: qsTr("Закрыть текущую вкладку")
        onTriggered: {
            closeButtonClicked(idTabView.currentIndex)
        }
    }    
//    Component.onDestruction: {
//        while(idTabView.count>0){
//            console.log("delete tab");
//            closeButtonClicked(0);
//        }
//    }
}

