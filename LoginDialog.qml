import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2

Dialog {
    id: loginDialog
    title: qsTr("Авторизация")
    width: 240
    height: 130    
    contentItem: Rectangle {        
        color: rootWindow.colorList.getColor("background")|| "steelblue"
        Label {
            id: helpLabel
            text: qsTr("Введите логин и пароль")
            font.pointSize: rootWindow.fontSize1
            anchors {
                margins: rootWindow.margins
                horizontalCenter: parent.horizontalCenter
                top: parent.top
            }

        }
        TextField {
            id: loginField
            placeholderText: qsTr("Логин")
            anchors {
                margins: rootWindow.margins
                horizontalCenter: parent.horizontalCenter
                top: helpLabel.bottom
            }            
        }
        TextField {
            id: passwordField
            placeholderText: qsTr("Пароль")
            echoMode: TextInput.Password
            anchors {
                margins: rootWindow.margins
                horizontalCenter: parent.horizontalCenter
                top: loginField.bottom
            }
        }

        Button {
            id: loginBtn
            action: loginAction
            anchors {
                margins: rootWindow.margins
                right: parent.horizontalCenter
                top: passwordField.bottom
            }
        }
        Button {
            id: cancelBtn
            action: cancelAction
            anchors {
                margins: rootWindow.margins
                left: parent.horizontalCenter
                top: passwordField.bottom
            }
        }

        Action {
            id: loginAction
            enabled: loginDialog.visible
            text: qsTr("Ок")
            shortcut: !cancelBtn.activeFocus?"Enter":""
            tooltip: qsTr("Подключиться к базе данных")
            onTriggered: {
                dBases.closeConnection()
                rootWindow.statusLabel1.text = qsTr("Не авторизован")
                rootWindow.connectSuccessful = false
                setDbParameter()
                if(!dBases.connectToDb()){
                    helpLabel.text = qsTr("Не могу подключиться к базе данных")
                    helpLabel.color = "red"
                    return
                }
                var queryStr = "SELECT count(*) FROM admins WHERE id LIKE '" + loginField.text + "'"
                var res = dBases.listFromDb(queryStr)
                if(res[0]!=1){
                    helpLabel.text = qsTr("Неправильное имя пользователя")
                    helpLabel.color = "red"
                    dBases.closeConnection()
                    return
                }
                queryStr = "SELECT DECODE(password, '" + Qt.atob(rootWindow.connectParam["keyToDb"]) +
                           "'), disable FROM admins WHERE id LIKE '" + loginField.text + "'"
                res = dBases.variantListFromDb(queryStr)
                if(res[0][0]!=passwordField.text||Number(res[0][1])!=0){
                    helpLabel.text = qsTr("Неправильный пароль")
                    helpLabel.color = "red"
                    dBases.closeConnection()
                    return
                }                
//                rootWindow.password = passwordField.text
                loginDialog.close()                
                passwordField.text = ""
                rootWindow.connectSuccessful = true
                rootWindow.user = loginField.text
                queryStr = "SELECT aid, name FROM admins WHERE id LIKE '" + loginField.text + "'"
                res = dBases.variantListFromDb(queryStr)
                rootWindow.userId = res[0][0]
                rootWindow.userFio = res[0][1] || rootWindow.user
                queryStr = "SELECT actions FROM admin_permits WHERE aid=" + rootWindow.userId +
                           " AND  section=1"
                rootWindow.userPermitPayment = dBases.listFromDb(queryStr)
                rootWindow.statusLabel1.text = rootWindow.userFio
            }
        }
        Action {
            id: login2Action
            enabled: loginDialog.visible
            shortcut: !cancelBtn.activeFocus?"return":""
            onTriggered: {
                loginAction.trigger()
            }
        }
        Action {
            id: cancelAction
            enabled: loginDialog.visible
            text: qsTr("Отмена")
            shortcut: "Esc"
            tooltip: qsTr("Закрыть окно подключения к базе данных")
            onTriggered: {
//                dBases.closeConnection()
                loginDialog.close()                
            }
        }
    }
    onVisibleChanged: {
        if(loginDialog.visible){
            helpLabel.text = qsTr("Ведите логин и пароль")
            helpLabel.color = "black"
            loginField.forceActiveFocus()
        }
    }
    function setDbParameter()
    {
        dBases.hostName = rootWindow.connectParam["hostName"] || "";
        dBases.dbName = rootWindow.connectParam["dbName"] || "";
        dBases.userName = rootWindow.connectParam["userName"] || "";
        dBases.userPassword = Qt.atob(rootWindow.connectParam["password"]) || "";
        if(rootWindow.connectParam["port"] !== undefined){
            dBases.dbPort = Number(rootWindow.connectParam["port"]);
        } else {
            dBases.setDbPort();
        }

        if(rootWindow.connectParam["dbType"]!=undefined){
            dBases.dbType = rootWindow.connectParam["dbType"];
        } else {
            dBases.setDbType();
        }

    }

    function encText(text, keyText)
    {
        keyText = keyText||"sJHkJsd7xzv<KGjdsbtRkjh8";
        var result = "";
//        console.log(text, "XOR=")
        for(var i=0, j=0;i<text.length;++i, ++j){
            if(j>=keyText.length) j=0;
            result += String.fromCharCode(text.charCodeAt(i)^keyText.charCodeAt(j));
//            console.log(result[i])
        }
        return result;
    }

}

