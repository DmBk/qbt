#ifndef PSUIPROXY_H
#define PSUIPROXY_H

#include <QObject>
#include <QDebug>
#include "pssqltablemodel.h"
#include "pssqlquerymodel.h"
#include "qdbases.h"
#include <QQmlContext>
#include <QQmlApplicationEngine>
#include <QQmlComponent>

//#include <QSqlDatabase>

class PsUiProxy : public QObject
{
    Q_OBJECT
public:
    explicit PsUiProxy(QObject *parent = 0);
    ~PsUiProxy();    
//    void createModel();
private:    
    QObject *qmlView = NULL;
    QQmlApplicationEngine engine;
    QQmlComponent *component = NULL;
    QDBases dbases;

signals:

public slots:
//    void newTab (QString objName);
    void listChild (QObject *qmlItem);

};

#endif // PSUIPROXY_H
