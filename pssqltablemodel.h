#ifndef PSSQLTABLEMODEL_H
#define PSSQLTABLEMODEL_H

#include <QSqlRelationalTableModel>
#include <QSqlRecord>
#include <QMetaObject>
#include <QDebug>
//#include <QtQuick>

class PsSqlTableModel : public QSqlRelationalTableModel
{
    Q_OBJECT
    Q_PROPERTY(int sortColumn READ sortColumn WRITE setSortColumn)
    Q_PROPERTY(QString table READ table WRITE setTable)
    Q_PROPERTY(Qt::SortOrder sortOrder READ sortOrder WRITE setSortOrder)
//    Q_PROPERTY(QByteArray filterRole READ filterRole WRITE setFilterRole)
//    Q_PROPERTY(QString filterString READ filterString WRITE setFilterString)


private:
    QHash<int, QByteArray> roles;


public:
    explicit PsSqlTableModel(QObject *parent = 0);
    ~PsSqlTableModel();
    Q_INVOKABLE QVariant data(const QModelIndex &index, int role=Qt::DisplayRole ) const;
    virtual QHash<int, QByteArray> roleNames() const{return roles;}
    Q_INVOKABLE void generateRoleNames();
//    Q_INVOKABLE bool select();
    Q_INVOKABLE void setRelationTable(int column, const QString &table, const QString &indexCol, const QString &displayCol);
//    Q_INVOKABLE void generateColumn(QObject &qmlObject);
    Q_INVOKABLE QStringList columnList();    
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    Q_INVOKABLE QString selectStatement() const;
    Q_INVOKABLE void addVirtualColumn(const QString &name);

    int sortColumn() const;
    void setSortColumn(int column);

    QString table() const;
    void setTable(const QString &name);

    Qt::SortOrder sortOrder() const;
    void setSortOrder(const Qt::SortOrder order);

//    QByteArray filterRole() const;
//    void setFilterRole(const QByteArray &role);

//    QString filterString() const;
//    void setFilterString(const QString &filter);

private:
int m_sortColumn;
Qt::SortOrder m_sortOrder;// = Qt::AscendingOrder;
QStringList m_virtualColumn;
int m_realColumnCount;

signals:

public slots:


};

#endif // PSSQLTABLEMODEL_H
