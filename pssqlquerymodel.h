#ifndef PSSQLQUERYMODEL_H
#define PSSQLQUERYMODEL_H


#include <QSqlQueryModel>
#include <QSqlQuery>
#include <QDebug>
#include <QSqlDatabase>
#include <QMetaObject>
#include <QSqlRecord>
//#include <QHash>

class PsSqlQueryModel : public QSqlQueryModel
{
    Q_OBJECT    
    Q_PROPERTY(QString selectString READ selectString WRITE setSelectString NOTIFY selectStringChanged)
//    Q_PROPERTY(QString selectString READ selectString WRITE setSelectString)
    Q_PROPERTY(bool strictFilter READ strictFilter WRITE setStrictFilter)
//    Q_PROPERTY(QString sortRole READ sortRole WRITE setSortRole)


public:
    explicit PsSqlQueryModel(QObject *parent = 0);
    ~PsSqlQueryModel();
    Q_INVOKABLE QVariant data(const QModelIndex &index, int role=Qt::DisplayRole ) const;
    Q_INVOKABLE virtual QHash<int, QByteArray> roleNames() const{return roles;}
//    Q_INVOKABLE void generateRoleNames();
    void setSelectString(const QString &select);
    QString selectString() const;
    bool strictFilter() const;
    void setStrictFilter(const bool strict);
    Q_INVOKABLE void setSortRoles(const QString &role, Qt::SortOrder order = Qt::AscendingOrder);
    QString sortRoles() const;    
    Q_INVOKABLE void setFilterRoles(QString role=QString(), const QString &value=QString());
    Q_INVOKABLE QString filterRoles() const;
    Q_INVOKABLE void clearFilter();
    Q_INVOKABLE void setSearchRoles(QString role=QString(), const QString &value=QString(),
                                    const bool &isDate=false);
    Q_INVOKABLE QString searchRoles() const;
    Q_INVOKABLE void clearSearch();
    Q_INVOKABLE QString fullQuery(const bool widthSort=true, const bool widthSearch=true,
                                  const bool widthFilter=true, const bool widthSelect=true) const;
    Q_INVOKABLE void setFullQuery();
    Q_INVOKABLE QVariantList rowToList(const int row) const;
    Q_INVOKABLE QVariant itemFromRow (const int row, const QString role) const;
    Q_INVOKABLE void setMultiRole(const QString &role, const QString &mainTable);
    Q_INVOKABLE void clearMultiRole();

private:
    QHash<int, QByteArray> roles;
    QString m_select;    
    QHash<QString,QString> h_filter;
    QHash<QString,QString> h_search;
    QHash<QString,QString> h_dateSearch;
    QHash<QString,QString> h_multiRole;
    QString m_order;
    QSqlDatabase* p_db;
    bool m_strictFilter;

signals:
    void selectStringChanged();
public slots:
    void generateRoleNames();

};

#endif // PSSQLQUERYMODEL_H
