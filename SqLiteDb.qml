import QtQuick 2.5
import QtQuick.LocalStorage 2.0

Item {
    property var db
    property string dbName: "qbtConf"
    property string version: "1.1"
    property string description: "qBT config database"
    property int estimSize: 1024000
    property string user

    function openDb()
    {
        try {
            db = LocalStorage.openDatabaseSync(dbName, version, description, estimSize);
        }
        catch(err) {
            console.log("Ошибка БД", err)
            try {
                console.log("try #2");
                db = LocalStorage.openDatabaseSync(dbName, "1.0", description, estimSize);
                db.changeVersion("1.0",version,
                                 function(tx){
                                     console.log("function 1");
                                     var query = "SELECT * FROM connect_param";
                                     var param = new Object();
                                     query = tx.executeSql(query);
                                     for(var i = 0; i < query.rows.length; ++i) {
                                         param[query.rows.item(i).param] = query.rows.item(i).value;
//                                         console.log(query.rows.item(i).param, query.rows.item(i).value);
                                     }
                                     if(param["userName"]!=undefined){
                                         tx.executeSql("INSERT OR REPLACE INTO connect_param (param, value) VALUES (?, ?)",
                                                       ["userName", rootWindow.loginDialog.encText(Qt.btoa(param["userName"]))]);
                                     }
                                     if(param["password"]!=undefined){
                                         param["password"] = Qt.atob(param["password"]);
                                         tx.executeSql("INSERT OR REPLACE INTO connect_param (param, value) VALUES (?, ?)",
                                                       ["password", rootWindow.loginDialog.encText(Qt.btoa(param["password"]),param["userName"])]);
                                     }
                                     if(param["keyToDb"]!=undefined){
                                         param["keyToDb"] = Qt.atob(param["keyToDb"]);
                                         tx.executeSql("INSERT OR REPLACE INTO connect_param (param, value) VALUES (?, ?)",
                                                       ["keyToDb", rootWindow.loginDialog.encText(Qt.btoa(param["keyToDb"]),param["userName"])]);
                                     }
                                 }
                                     );
            }
            catch(err) {
                console.log("Ошибка БД", err)
            }
        }
    }

    function createTables()
    {
        //        console.log("createTable")
        db.transaction(
                    function(tx) {
                        // Create the database if it doesn't already exist
                        tx.executeSql('CREATE TABLE IF NOT EXISTS connect_param(param TEXT PRIMARY KEY, value TEXT)');
                        tx.executeSql('CREATE TABLE IF NOT EXISTS window(param TEXT PRIMARY KEY, value TEXT)');
                        tx.executeSql('CREATE TABLE IF NOT EXISTS payment_method(param TEXT PRIMARY KEY, value TEXT)');
                        tx.executeSql('CREATE TABLE IF NOT EXISTS inner_describe(param TEXT PRIMARY KEY, value TEXT)');
                        tx.executeSql('CREATE TABLE IF NOT EXISTS color(param TEXT PRIMARY KEY, value TEXT)');
                        tx.executeSql('CREATE TABLE IF NOT EXISTS free_ip(param TEXT PRIMARY KEY, value TEXT)');
                    }
                    )
    }

    function writeParam(table, param, value)
    {
        db.transaction(
                    function(tx){
                        tx.executeSql("INSERT OR REPLACE INTO " + table + " (param, value) VALUES (?, ?)", [param, value || 0])
                    }
                    )
    }

    //correction - adds the specific query parameters
    function readParam(table, param, correction)
    {
        var query = "SELECT * FROM " + table
        if(param != undefined){
            query += " WHERE param LIKE '" + param + "' "
            if(correction != undefined){
                query += correction
            }
        }
        var arr = new Object()
        db.readTransaction(
                    function(tx){
                        //                        console.log("query=", query)
                        query = tx.executeSql(query);
                        for(var i = 0; i < query.rows.length; ++i) {
                            arr[query.rows.item(i).param] = query.rows.item(i).value
                        }
                    }
                    )
        //        console.log("arr=", Qt.atob(arr["password"]))
        return arr
    }

    function deleteParam(table, param)
    {
        db.transaction(
                    function(tx){                        
                        tx.executeSql("DELETE FROM " + table +
                                      (param != undefined?" WHERE param LIKE '" + param + "'": ""))
                    }
                    )
    }    

    Component.onCompleted: {
        openDb();
        createTables();
    }    

}

