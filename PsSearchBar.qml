import QtQuick 2.5
import QtQuick.Controls 1.4

ScrollView{
    id: idScrollView
    property alias listModel: idListModel
    property alias listView: idListView
    signal fieldTextChanged(string role, string value)
    ListView {
        id: idListView
        orientation: ListView.Horizontal
        model: ListModel {
            id: idListModel
        }
        delegate: TextField {
            width: elementWidth
            height: parent.height
            placeholderText: elementTitle
            onTextChanged: {                
                idScrollView.fieldTextChanged(elementRole, text)                
            }
        }        
    }
}
