#include "psuiproxy.h"

PsUiProxy::PsUiProxy(QObject *parent) :
    QObject(parent)
{
//    dbases = new QDBases;
    qmlRegisterType<PsSqlTableModel>("PsSqlTableModel", 0, 5, "PsSqlTableModel");
    qmlRegisterType<PsSqlQueryModel>("PsSqlQueryModel", 0, 7, "PsSqlQueryModel");
//    engine = new QQmlApplicationEngine(this);
    component = new QQmlComponent(&engine,QUrl(QStringLiteral("qrc:///main.qml")),this);
    engine.rootContext()->setContextProperty("dBases", &dbases);
    engine.setOfflineStoragePath(QCoreApplication::applicationDirPath());
//    engine.rootContext()->setContextProperty("applicationDirPath", QCoreApplication::applicationDirPath());
//    engine.rootContext()->setContextProperty("dirSeparator", QDir::separator());
//    engine.rootContext()->setContextProperty("myDb", dbases.database());
    qmlView = component->create();
    QObject::connect(qmlView, SIGNAL(myChild(QObject*)), this, SLOT(listChild(QObject*)));

//    createModel();
}
PsUiProxy::~PsUiProxy()
{
    qDebug()<<"PsUiProxy destructor";

//    delete tableModel;
    delete qmlView;
    delete component;
//    delete engine;
//    delete dbases;
}

//void PsUiProxy::createModel()
//{
//    qDebug()<<engine->parent()<< "Engine parent";
////    QObject *qmlTabView = qmlView->findChild<QObject*>("myTabView");
////    foreach(const QObject *obj, qmlTabView->children()) {
////      qDebug() << obj;
////    }
//    tableModel = new PsSqlTableModel;
////    dbases = new QDBases;
//    qDebug()<<"tableModel parent =" << (tableModel->parent());
//    tableModel->setTable("admins");
//    tableModel->generateRoleNames();
//    tableModel->select();
//    engine->rootContext()->setContextProperty("tableModel", tableModel);
//    connect(qmlView, SIGNAL(newTabCreated(QString)), this, SLOT(newTab(QString)));
//}

//void PsUiProxy::newTab(QString objName)
//{
//    QObject *qmlTableView = qmlView->findChild<QObject*>(objName);
//    if (qmlTableView) {
//        tableModel->generateColumn(*qmlTableView);
//    } else {
//        qDebug() << "Не нашел " << objName;
//    }
//}

void PsUiProxy::listChild(QObject *qmlItem)
{
//    QObject *qmlItem = qmlView->findChild<QObject*>(objName);
    foreach(QObject *obj, qmlItem->children()) {

          qDebug() << obj;
          listChild(obj);
        }
}
