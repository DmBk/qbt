import QtQuick 2.7
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4

Dialog {
    id: paymentPack
    property alias packName: packName
    property alias payNumber: payNumber
    property alias paySum: paySum
    title: qsTr("Начать ввод пачки")
    standardButtons: StandardButton.Ok | StandardButton.Cancel
        ColumnLayout {
            anchors {
                margins: rootWindow.margins
            }
            spacing: rootWindow.margins
            TextField {
                id: packName
                focus: true
                placeholderText: qsTr("Имя пачки:")
                Layout.fillWidth: true
            }
            Label {
                text: qsTr("Платежи:")
            }
            RowLayout {
                TextField {
                    id: payNumber                    
                    placeholderText: qsTr("Количество")
                    Layout.fillWidth: true
                    validator: IntValidator {
                        bottom: 0
                        top: 99999
                        locale: Qt.locale().name
                    }

                    onEditingFinished: {
                        text = Number.fromLocaleString(Qt.locale(), text).toLocaleString(Qt.locale(), "f", 0)
                    }
                }
                TextField {
                    id: paySum                    
                    placeholderText: qsTr("Сумма")
                    Layout.fillWidth: true
                    validator: DoubleValidator {
                        bottom: 0.00
                        top: 999999.99
                        decimals: 2
                        notation: DoubleValidator.StandardNotation
                    }
                    Keys.onPressed: {
                        priority: Keys.BeforeItem
                        if(event.key === Qt.Key_Period || event.key === Qt.Key_Comma){
                            if((text.toString().indexOf(",") < 0) && (text.toString().indexOf(".") < 0)){
                                text = text.toString() + Qt.locale().decimalPoint
                            }
                            event.accepted = true
                        }
                    }
                    onEditingFinished: {
                        var textWithoutSeparator = text.toString().replace(Qt.locale().groupSeparator, '')
                        text = Number.fromLocaleString(Qt.locale(), textWithoutSeparator).toLocaleString(Qt.locale())
                    }
                }
            }
        }
        onVisibleChanged: {
            if (visible) {
                packName.forceActiveFocus()
            }
        }
}
