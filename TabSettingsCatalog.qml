import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1

Item {
    id: catalogSettings
    ScrollView {
        id: scrollView
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
            bottom: rowLayout.top
            margins: rootWindow.margins
        }

        GridLayout {
            columns: 2
            Label {
                text: qsTr("Внутреннее описание:")
            }
            Label {
                text: qsTr("Вид оплаты:")
            }
            RowLayout {
                id: rowInnerDescribe
                TextField {
                    id: innerDescribeField
                    implicitWidth: 140
                    placeholderText: qsTr("Название")
                }
                Button {
                    text: qsTr("Добавить")
                    Keys.onReturnPressed: clicked()
                    Keys.onEnterPressed: clicked()
                    onClicked: {
                        innerDescribeList.arrDescribe.push(innerDescribeField.text);
                        innerDescribeList.modelChanged();
                    }
                }
            }
            RowLayout {
                id: rowPaymentMethod
                TextField {
                    id: paymentNameField
                    implicitWidth: 110
                    placeholderText: qsTr("Название")
                }
                TextField {
                    id: paymentNumField
                    implicitWidth: 25
                    placeholderText: qsTr("№")
                    validator: IntValidator {bottom: 0; top: 999;}
                }

                Button {
                    text: qsTr("Добавить")
                    Keys.onReturnPressed: clicked()
                    Keys.onEnterPressed: clicked()
                    onClicked: {
                        paymentMethod.append({"text":paymentNameField.text||" ", "num":Number(paymentNumField.text)})
                    }
                }
            }
            TableView {
                id: innerDescribeList
                property var arrDescribe: new Array()
                property int clickedRow: -1
//                headerVisible: false
                implicitWidth: rowInnerDescribe.implicitWidth
                implicitHeight: 220

                TableViewColumn {
                    role: "button"
                    width: 10
                    delegate: Button {
                        width: 10
                        iconSource: "images/sort-up.png"
                        onClicked: {
                            innerDescribeList.upItem(styleData.row);
                            innerDescribeList.modelChanged()
                        }
                    }
                }
                TableViewColumn {
//                    title: qsTr("Название")
                    //                    role: "describe"
                }
                model: arrDescribe

                //                itemDelegate: Text {
                //                    text: styleData.role=="describe"?rootWindow.arrInnerDescribe[styleData.row]:""
                //                }
                Menu {
                    id: contextMenu
                    title: qsTr("Удалить")
                    MenuItem {
                        //                        enabled:
                        text: qsTr("Удалить")
                        //                        shortcut: "Del"
                        onTriggered: {
                            innerDescribeList.arrDescribe.splice(innerDescribeList.clickedRow,1);
                            innerDescribeList.modelChanged();
                        }
                    }
                }

                rowDelegate: Rectangle {
                    color: !styleData.selected?
                               (styleData.alternate&&innerDescribeList.alternatingRowColors?
                                    rootWindow.colorList.getColor("tableViewAlternatingRow"):
                                    rootWindow.colorList.getColor("tableViewRow")):
                               rootWindow.colorList.getColor("tableViewSelectedRow")
                    MouseArea {
                        anchors.fill: parent
                        //                    acceptedButtons: Qt.LeftButton | Qt.RightButton
                        acceptedButtons: Qt.RightButton
                        onClicked: {
                            if (mouse.button == Qt.RightButton){
                                innerDescribeList.clickedRow = styleData.row
                                contextMenu.popup()
                            }
                        }
                    }
                }
                function upItem(index)
                {
                    var itm = arrDescribe[index]
                    if(index!=0){
                        arrDescribe[index] = arrDescribe[index-1]
                        arrDescribe[index-1] = itm
                    }
                }
            }
            TableView {
                id: paymentMethodList
//                property var arrMethod: new Array()
                property int clickedRow: -1
                property alias paymentMethod: paymentMethod
                implicitWidth: innerDescribeList.implicitWidth
                implicitHeight: innerDescribeList.implicitHeight
//                headerVisible: false

                TableViewColumn {
                    role: "button"
                    width: 10
                    delegate: Button {
                        width: 10
                        iconSource: "images/sort-up.png"
                        onClicked: {
                            paymentMethodList.upItem(styleData.row);
                            paymentMethodList.modelChanged()
                        }
                    }
                }
                TableViewColumn {
//                    title: qsTr("Название")
                    role: "text"
                }
                TableViewColumn {
//                    title: qsTr("Номер")
                    role: "num"
                    width: 30
                }

                model: ListModel {
                    id: paymentMethod
                }

                Menu {
                    id: paymentContextMenu
                    title: qsTr("Удалить")
                    MenuItem {
                        text: qsTr("Удалить")
                        //                        shortcut: "Del"
                        onTriggered: {
                            paymentMethodList.paymentMethod.remove(paymentMethodList.clickedRow,1);
                            paymentMethodList.modelChanged();
                        }
                    }
                }

                rowDelegate: Rectangle {
                    color: !styleData.selected?
                               (styleData.alternate&&paymentMethodList.alternatingRowColors?
                                    rootWindow.colorList.getColor("tableViewAlternatingRow"):
                                    rootWindow.colorList.getColor("tableViewRow")):
                               rootWindow.colorList.getColor("tableViewSelectedRow")
                    MouseArea {
                        anchors.fill: parent
                        acceptedButtons: Qt.RightButton
                        onClicked: {
                            if (mouse.button == Qt.RightButton){
                                paymentMethodList.clickedRow = styleData.row;
                                paymentContextMenu.popup();
                            }
                        }
                    }
                }
                function upItem(index)
                {
                    if(index>0){
                        paymentMethodList.paymentMethod.move(index,index-1,1);
                    }
                }
            }
        }
    }

    RowLayout {
        id: rowLayout
        anchors {
            right: parent.right
            bottom: parent.bottom
            margins: rootWindow.margins
        }
        Button {
            id: applyButton
            text: qsTr("Применить")
            Keys.onReturnPressed: clicked()
            Keys.onEnterPressed: clicked()
            onClicked: {
                rootWindow.arrInnerDescribe.length = 0;
                rootWindow.configDb.deleteParam("inner_describe");
                for(var i in innerDescribeList.arrDescribe){
                    rootWindow.arrInnerDescribe[i] = innerDescribeList.arrDescribe[i];
                    rootWindow.configDb.writeParam("inner_describe", rootWindow.arrInnerDescribe[i]);
                }
                if(paymentMethod.count<rootWindow.paymentMethodModel.count)
                    rootWindow.paymentMethodModel.clear();
                for(var i=0;i<paymentMethod.count;++i){
                    rootWindow.paymentMethodModel.set(i,paymentMethodList.paymentMethod.get(i));
                    rootWindow.configDb.writeParam("payment_method", paymentMethod.get(i).text, paymentMethod.get(i).num.toString());
                }
            }
        }
        Button {
            id: cancelButton
            text: qsTr("Отмена")
            Keys.onReturnPressed: clicked()
            Keys.onEnterPressed: clicked()
            onClicked: {
                if(rootWindow.arrInnerDescribe.length<innerDescribeList.arrDescribe.length)
                    innerDescribeList.arrDescribe.length = 0
                for(var i in rootWindow.arrInnerDescribe){
                    innerDescribeList.arrDescribe[i] = rootWindow.arrInnerDescribe[i];
                }
                innerDescribeList.modelChanged();
                if(rootWindow.paymentMethodModel.count<paymentMethod.count)
                    paymentMethod.clear();
                for(var i=0;i<rootWindow.paymentMethodModel.count;++i){
                    paymentMethod.set(i,rootWindow.paymentMethodModel.get(i));
                }
                paymentMethodList.modelChanged();
            }
        }
        Button {
            id: restoreToDefaultButton
            Keys.onReturnPressed: clicked()
            Keys.onEnterPressed: clicked()
            text: qsTr("Стандартные настройки")
        }
    }

    Component.onCompleted: {
        for(var i in rootWindow.arrInnerDescribe){
            innerDescribeList.arrDescribe[i] = rootWindow.arrInnerDescribe[i];
        }
        for(var i=0;i<rootWindow.paymentMethodModel.count;++i){
            paymentMethodList.paymentMethod.set(i,rootWindow.paymentMethodModel.get(i));
        }
        innerDescribeList.modelChanged();
        paymentMethodList.modelChanged();
    }
}

