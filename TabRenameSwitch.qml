import QtQuick 2.5
import QtQuick.Controls 1.4

Item {
    id: itemRenameSwitch
    objectName: "objTabRenameSwitch"
    property int margins: 5
    property int fontSizeTop: 12
    property int fontSize1: 11
    property int fontSize2: 10
    Label {
        id: idLabel1
        text: qsTr("Замена IP адреса свича")
        font.pointSize: parent.fontSizeTop
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.margins: parent.margins
    }
    Label {
        id: idLabel2
        text: qsTr("Старый свич")
        anchors.top: idLabel1.bottom
        anchors.horizontalCenter: oldIp.horizontalCenter
        font.pointSize: parent.fontSize2
        anchors.margins: parent.margins
    }
    Label {
        id: idLabel3
        text: qsTr("Новый свич")
        anchors.top: idLabel1.bottom
        anchors.horizontalCenter: newIp.horizontalCenter
        font.pointSize: parent.fontSize2
        anchors.margins: parent.margins
    }
    IpAddrField {
        id: oldIp
        focus: true
        //        text: "10.200.6.22"
        anchors.top: idLabel2.bottom
        anchors.right: parent.horizontalCenter
        anchors.margins: parent.margins
        onTextChanged: {
            parent.viewIp(oldIp.text, idTextOldIp);
        }
    }
    IpAddrField {
        id: newIp
        anchors.top: idLabel2.bottom
        anchors.left: parent.horizontalCenter
        anchors.margins: parent.margins
        onTextChanged: {
            parent.viewIp(newIp.text, idTextNewIp);
        }
    }
    Button {
        text: qsTr("Заменить")
        anchors {
            left: newIp.right
            top: newIp.top
            bottom: newIp.bottom
            leftMargin: parent.margins
            topMargin: 0
            bottomMargin: 0
        }
        Keys.onReturnPressed: parent.renameSwitch()
        Keys.onEnterPressed: parent.renameSwitch()
        onClicked: parent.renameSwitch()
        tooltip: qsTr("Заменить")
    }
    Button {
        id: idCheck
        text: checked? "/":""
        tooltip: qsTr("\"/\" - для устройств с выделенными абонентскими портами (свичи)\n
                      Неактивно - для устройств без выделенных портов (HPNA, GePON)")
        width: 10
        checkable: true
        anchors {            
            horizontalCenter: parent.horizontalCenter
            top: oldIp.top
            bottom: oldIp.bottom
            rightMargin: parent.margins
            leftMargin: parent.margins
            topMargin: 0
            bottomMargin: 0
        }
        onClicked: {
            parent.viewIp(oldIp.text, idTextOldIp);
            parent.viewIp(newIp.text, idTextNewIp);
        }        
    }

    TextArea {
        id: idTextOldIp
        font.pointSize: parent.fontSize1
        readOnly: true
        backgroundVisible: false
        anchors {
            top: oldIp.bottom
            right: oldIp.right
            //            bottom: parent.bottom
            topMargin: parent.margins
            bottomMargin: parent.margins
            rightMargin: 0
            leftMargin: parent.margins
        }
    }
    TextArea {
        id: idTextNewIp
        font.pointSize: parent.fontSize1
        readOnly: true
        backgroundVisible: false
        anchors {
            top: oldIp.bottom
            left: newIp.left
            //            bottom: parent.bottom
            topMargin: parent.margins
            bottomMargin: parent.margins
            rightMargin: parent.margins
            leftMargin: 0
        }
    }

    Component.onCompleted: {
        idCheck.checked = true
    }

    function viewIp(ip, item)
    {
        if (ip.search(/[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}/) != -1) {
            if (typeof item !== 'undefined') {
                var query = "SELECT _switch, CONCAT(city, ',', address_street, ',', address_build, ',', address_flat) as address
                             FROM users_pi
                             WHERE users_pi._switch LIKE '" + ip + idCheck.text + (idCheck.checked?"%'":"'") +
                             " ORDER BY ABS(SUBSTRING_INDEX(users_pi._switch,'/',-1));";
                query = dBases.variantListFromDb(query);
                item.text = "";
                for (var i=0; i<query.length; ++i) {
                    item.append(query[i][0] + " " + query[i][1]);
                }
                item.cursorPosition = 0;
                heightChanged();
                widthChanged();
            }
            return true;
        } else {
            return false;
        }        
    }

    function renameSwitch()
    {
        if (viewIp(oldIp.text) && viewIp(newIp.text)) {
            var query1 = "SELECT SUBSTRING_INDEX(USER(),'@',-1);";
            query1 = dBases.listFromDb(query1);
            var query2 = "INSERT INTO `admin_actions` (actions, datetime, ip, uid, aid, action_type)
                          SELECT concat('_switch ', '" + oldIp.text + idCheck.text + "', mid(users_pi._switch, length('" + oldIp.text +
                          idCheck.text + "') + 1), '->','" + newIp.text + idCheck.text + "', mid(users_pi._switch, length('" +
                          oldIp.text + idCheck.text +"') + 1))
                          , now()
                          , inet_aton('" + query1[0] + "')
                          , uid
                          , " + rootWindow.userId +
                          ", 2
                          FROM users_pi
                          WHERE users_pi._switch LIKE '" + oldIp.text + idCheck.text + (idCheck.checked?"%'":"'") + ";";
            var query3 = "UPDATE `users_pi`
                          SET users_pi._switch = concat('" + newIp.text + idCheck.text + "', mid(users_pi._switch, length('" +
                          oldIp.text + idCheck.text + "') + 1))
                          WHERE users_pi._switch LIKE '" + oldIp.text + idCheck.text + (idCheck.checked?"%'":"'") + ";";
//            console.log(query1[0], query2);
//            console.log(query3);
            dBases.listFromDb(query2);
            dBases.listFromDb(query3);
            viewIp(oldIp.text, idTextOldIp);
            viewIp(newIp.text, idTextNewIp);
        }
    }

    onHeightChanged: {
        idTextOldIp.height = Math.min(Math.max(idTextOldIp.contentHeight + 2, idTextOldIp.height),
                                      (itemRenameSwitch.height - idLabel1.height - idLabel2.height - oldIp.height - 5 * itemRenameSwitch.margins));
        idTextNewIp.height = Math.min(Math.max(idTextNewIp.contentHeight + 2, idTextNewIp.height),
                                      (itemRenameSwitch.height - idLabel1.height - idLabel2.height - oldIp.height - 5 * itemRenameSwitch.margins));
        idTextOldIp.height = idTextNewIp.height = Math.max(idTextOldIp.height, idTextNewIp.height)
        //        console.log("idText.height=", idText.height);
    }
    onWidthChanged: {
        idTextOldIp.wrapMode = idTextNewIp.wrapMode = TextEdit.NoWrap;
        idTextOldIp.width = Math.min(Math.max(idTextOldIp.contentWidth + 2, idTextOldIp.width),
                                     (oldIp.x + oldIp.width - itemRenameSwitch.margins));
        idTextNewIp.width = Math.min(Math.max(idTextNewIp.contentWidth + 2, idTextNewIp.width),
                                     (itemRenameSwitch.width - newIp.x - newIp.width - itemRenameSwitch.margins));
        idTextOldIp.width = idTextNewIp.width = Math.max(idTextOldIp.width, idTextNewIp.width);
        idTextOldIp.wrapMode = idTextNewIp.wrapMode = TextEdit.WordWrap;
        //        console.log("idText.width=", idText.width);
    }
}
