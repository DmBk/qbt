import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1
import PsSqlQueryModel 0.7

Dialog {
    id: addPaymentDialog
    property int uid: -1
    property string fio
    property string address
    property string dogovor
    property string dateDogovor
    property bool firstSessionRun: true
    //property string lastPack: "0"
    property real paymentSum: Number.fromLocaleString(Qt.locale(), paySum.text.toString())
    property alias packField: packField
    //    property real sumPack: 0
    //    property var packArr: new Array()
    signal refreshPayment()
    signal paymentAdded()
    title: qsTr("Внести платеж")
    width: infoLayout.width + 2*rootWindow.margins
    height: infoLayout.implicitHeight + 8*rootWindow.margins + splitRect.implicitHeight +
            payLayout.implicitHeight + addButton.implicitHeight + paymentView.implicitHeight

    contentItem: Rectangle {
        color: rootWindow.colorList.getColor("background") || "steelblue"
        GridLayout {
            id: infoLayout
            columns: 2
            anchors {
                margins: rootWindow.margins
                top: parent.top
                left: parent.left
            }

            Label {
                text: "Л/С:"
            }
            RowLayout{
                id: rowLayot
                TextField {
                    id: idField1
                    text: uid
                    readOnly: true
                    activeFocusOnTab: false
                    implicitWidth: Math.max(__contentWidth + 10, 60)
                    font.pointSize: rootWindow.fontSize2
                }
                Label {
                    text: qsTr("Договор:")
                }
                TextField {
                    text: dogovor
                    readOnly: true
                    activeFocusOnTab: false
                    implicitWidth: Math.max(__contentWidth + 10, 60)
                    font.pointSize: idField1.font.pointSize
                }
                Label {
                    text: qsTr("Дата:")
                }
                TextField {
                    text: dateDogovor
                    readOnly: true
                    activeFocusOnTab: false
                    implicitWidth: Math.max(__contentWidth + 10, 80)
                    font.pointSize: idField1.font.pointSize
                }
            }
            Label {
                text: qsTr("ФИО:")
            }
            TextField {
                id: infoFio
                text: fio
                readOnly: true
                activeFocusOnTab: false
                implicitWidth: Math.min(Math.max(__contentWidth + 10, rowLayot.implicitWidth), 1000)
                font.pointSize: idField1.font.pointSize
            }
            Label {
                text: qsTr("Адрес:")
            }
            TextField {
                text: address
                readOnly: true
                activeFocusOnTab: false
                implicitWidth: Math.min(Math.max(__contentWidth + 10, rowLayot.implicitWidth, infoFio.implicitWidth), 1000)
                font.pointSize: idField1.font.pointSize
            }

        }
        Rectangle {
            id: splitRect
            width: parent.width
            height: 3
            color: "silver"
            anchors {
                margins: rootWindow.margins
                top: infoLayout.bottom
            }
        }
        GridLayout {
            id: payLayout
            columns: 2
            anchors {
                top: splitRect.bottom
                left: parent.left
                //                right: parent.right
                margins: rootWindow.margins
            }

            Label {
                id: payLabel1
                text: qsTr("Сумма:")
            }
            RowLayout {
                id: payRow1
                TextField {
                    id: paySum
                    focus: true
                    implicitWidth: 64
                    font.pointSize: idField1.font.pointSize
                    horizontalAlignment: Qt.AlignRight
                    validator: DoubleValidator {
                        bottom: 0.01
                        top: 99999.99
                        decimals: 2
                        notation: DoubleValidator.StandardNotation
                    }
                    Keys.onPressed: {
                        priority: Keys.BeforeItem                       
                        if(event.key==Qt.Key_Period||event.key==Qt.Key_Comma){
                            //                            console.log(event.text)
                            if((text.toString().indexOf(",")<0)&&(text.toString().indexOf(".")<0)){
                                text = text.toString() + Qt.locale().decimalPoint
                            }
                            event.accepted = true
                        }
                    }
                }
                Label {
                    id: payLabel2
                    text: qsTr("Оплачено на:")
                }
                ComboBox {
                    id: companyBox
                    model: [qsTr("Неопределено"), qsTr("Артнет"), qsTr("Артнет Плюс"), qsTr("Артмедиа")]
                    implicitWidth: infoLayout.implicitWidth - 9*rootWindow.margins -
                                   payLabel1.width - paySum.width - payLabel2.width
                }
            }
            Label {
                text: qsTr("Описание:")
            }
            TextField {
                id: describeField
                implicitWidth: payRow1.implicitWidth
                font.pointSize: idField1.font.pointSize
            }
            Label {
                text: qsTr("Внутреннее:")
            }
            ComboBox {
                id: innerDescribeBox
                editable: true
                activeFocusOnPress: true
                implicitWidth: payRow1.implicitWidth
//                model: [qsTr(" "), qsTr("Почта "), qsTr("Приватбанк "), qsTr("Ощадбанк "), qsTr("Сбербанк России "),
//                    qsTr("Банк Национальный Кредит "), qsTr("Финансы и Кредит "), qsTr("ПУМБ "), qsTr("Укрсоцбанк "),
//                    qsTr("Правекс Банк "), qsTr("Креди Агриколь Банк ")]
                model: rootWindow.arrInnerDescribe
            }
            Label {
                text: qsTr("Вид оплаты:")
            }
            RowLayout {
                id: payRow4
                ComboBox {
                    id: methodBox
                    implicitWidth: 160
                    model: rootWindow.paymentMethodModel
//                    model: ListModel {
//                        id: methodModel
//                        ListElement { text: qsTr("Банк"); num: 0 }
//                        ListElement { text: qsTr("Корректировка"); num: 5 }
//                        ListElement { text: qsTr("Компенсация"); num: 6 }
//                        ListElement { text: "Credit Card"; num: 3 }
//                        ListElement { text: "IPay"; num: 72 }
//                        ListElement { text: "WebMoney"; num: 41 }
//                        ListElement { text: qsTr("Внешние платежи"); num: 2 }
//                        ListElement { text: qsTr("Наличные"); num: 1 }
//                        ListElement { text: qsTr("Перевод личных средств"); num: 7 }
//                        ListElement { text: qsTr("Пересчитать"); num: 8 }
//                        ListElement { text: qsTr("Бонус"); num: 4 }
//                    }
                }
                CalendarBox {
                    id: calendarBox
                    anchors.top: parent.top
                    implicitWidth: payRow1.implicitWidth - methodBox.implicitWidth -
                                   payRow4.spacing// - calendarBox.height
//                    calendarX: addPaymentDialog.x + payLayout.x + payLayout.width
//                    calendarY: addPaymentDialog.y + payLayout.y + payLayout.height
                    calendarX: addPaymentDialog.x + payLayout.x + payRow4.x + calendarBox.x
                    calendarY: addPaymentDialog.y + payLayout.y + payRow4.y
                }

            }
        }
        TextField {
            id: packField
            placeholderText: qsTr("ИД пачки")
            width: payLayout.width - payRow1.width
            horizontalAlignment: Qt.AlignRight
            maximumLength: 30
            anchors {
                margins: rootWindow.margins
                left: parent.left
                top: payLayout.bottom
                //                right: addButton.left
            }
        }
        Button {
            id: addButton
            action: addAction
            anchors {
                margins: rootWindow.margins
                top: payLayout.bottom
                right: parent.horizontalCenter
            }
        }
        Button {
            id: cancelButton
            action: cancelAction
            anchors {
                margins: rootWindow.margins
                top: payLayout.bottom
                left: parent.horizontalCenter
            }
            Keys.onReturnPressed: cancelAction.trigger()
            Keys.onEnterPressed: cancelAction.trigger()
        }
        PsTableView {
            id: paymentView
            property int paymentId
            property real paymentSum
            enabled: addPaymentDialog.visible
            implicitHeight: 100
            anchors {
                topMargin: rootWindow.margins
                left: parent.left
                right: parent.right
                top: addButton.bottom
                bottom: parent.bottom
            }
            TableViewColumn { role: "id"; title: qsTr("#"); width: 45 }
            TableViewColumn { role: "date"; title: qsTr("Дата"); width: 65 }
            TableViewColumn { role: "sum"; title: qsTr("Сумма"); width: 45 }
            TableViewColumn { role: "last_deposit"; title: qsTr("Депозит"); width: 50 }
            TableViewColumn { role: "dsc"; title: qsTr("Описание"); width: 80 }
            TableViewColumn { role: "inner_describe"; title: qsTr("Внутреннее"); width: 120 }
            TableViewColumn { role: "reg_date"; title: qsTr("Регистрация"); width: 90 }
            TableViewColumn { role: "method"; title: qsTr("Вид"); width: 60 }
            TableViewColumn { role: "payment_account"; title: qsTr("Счет"); width: 60 }
            TableViewColumn { role: "name"; title: qsTr("Администратор"); width: 120; }
            model: PsSqlQueryModel {
                id: tableModel
                strictFilter: true
                selectString: "SELECT payments.id, payments.date, payments.sum, payments.last_deposit, payments.dsc,
                                      payments.inner_describe, payments.method, payments.reg_date, payments.payment_account,
                                      payments.ext_id, payments.bill_id, payments.uid, INET_NTOA(payments.ip), admins.name
                               FROM `payments`
                               LEFT JOIN admins ON payments.aid=admins.aid"
            }
            Menu {
                id: paymentViewContextMenu
                title: "Edit"
                MenuItem {
                    enabled: rootWindow.userPermitPayment.indexOf("2")+1
                    text: qsTr("Удалить")
                    //                        shortcut: "Del"
                    onTriggered: {
                        var query1 = "SELECT SUBSTRING_INDEX(USER(),'@',-1);"
                        query1 = dBases.listFromDb(query1)
                        var query2 = "SELECT `bills`.`id`,`bills`.`deposit` FROM `bills`, `users`
                                          WHERE `users`.`uid`=" + uid + " AND `users`.`bill_id`=`bills`.`id`;"
                        var res2 = dBases.variantListFromDb(query2)
                        var query3 = "INSERT INTO `admin_actions` (actions, datetime, ip, uid, aid, action_type)
                                         VALUES ( '" +
paymentView.paymentId + " " + paymentView.paymentSum + "',
                                         now(),
                                         inet_aton('" + query1[0] + "'), " +
addPaymentDialog.uid + ", " +
rootWindow.userId + ",
                                         16);
                                         DELETE FROM `payments` WHERE `id`=" +
paymentView.paymentId + " AND `uid`=" +
uid + ";
                                         UPDATE `bills` SET `deposit`=(`deposit` - " +
paymentView.paymentSum +
") WHERE  `id`=" + res2[0][0] + ";"
                        //                            console.log(query3)
                        dBases.listFromDb(query3)
                        tableModel.setFullQuery()
                        addPaymentDialog.refreshPayment()

                    }

                }
            }

            itemDelegate: Text {
//                width: parent.width
                anchors.margins: 4
//                anchors.left: parent.left
//                anchors.verticalCenter: parent.verticalCenter
                elide: Text.ElideRight
                clip: true
                text: if (styleData.value instanceof Date)
                          if(styleData.role=="date")
                              styleData.value.toLocaleString(Qt.locale(), "yyyy-MM-dd")
                          else styleData.value.toLocaleString(Qt.locale(), "yyyy-MM-dd HH:mm:ss")
                      else if(styleData.role == "sum" || styleData.role == "last_deposit")
                                            Number(styleData.value).toLocaleString(Qt.locale()) || "0"
                      else styleData.value
                color: styleData.textColor
            }

            rowDelegate: Rectangle {
                id: paymentViewDelegate
                color: !styleData.selected?
                           (styleData.alternate&&paymentView.alternatingRowColors?
                                rootWindow.colorList.getColor("tableViewAlternatingRow"):
                                rootWindow.colorList.getColor("tableViewRow")):
                           rootWindow.colorList.getColor("tableViewSelectedRow")
                MouseArea {
                    anchors.fill: parent
                    //                    acceptedButtons: Qt.LeftButton | Qt.RightButton
                    acceptedButtons: Qt.RightButton
                    onClicked: {
                        if (mouse.button == Qt.RightButton){
                            paymentView.paymentId = tableModel.itemFromRow(styleData.row,"id")
                            paymentView.paymentSum = tableModel.itemFromRow(styleData.row,"sum")
                            paymentViewContextMenu.popup()
                        }
                    }
                }
            }

            onEnabledChanged: {
                if(enabled){
                    if(firstSessionRun){
                        firstSessionRun = false
                        tableModel.setFilterRoles("uid", addPaymentDialog.uid)
                        paymentView.sortIndicatorColumn = 0
                        paymentView.sortIndicatorOrder = Qt.DescendingOrder
                        //                        tableModel.generateRoleNames()
                    } else {
                        tableModel.setFilterRoles("uid", addPaymentDialog.uid)
                        tableModel.setFullQuery()
                    }


                }
            }
            onSortIndicatorOrderChanged: {
                timer.stop()

                if(!paymentView.sortIndicatorVisible) {
                    paymentView.sortIndicatorVisible = true
                }

                var column = paymentView.getColumn(paymentView.sortIndicatorColumn)
                tableModel.setSortRoles(column.role, paymentView.sortIndicatorOrder)
                tableModel.setFullQuery();
            }

            onSortIndicatorColumnChanged: {
                timer.stop()

                if(!paymentView.sortIndicatorVisible) {
                    paymentView.sortIndicatorVisible = true
                }

                var column = paymentView.getColumn(paymentView.sortIndicatorColumn)
                tableModel.setSortRoles(column.role, paymentView.sortIndicatorOrder)
                tableModel.setFullQuery()
            }
        }

        Action {
            id: addAction
            enabled: addPaymentDialog.visible && paySum.acceptableInput
            text: qsTr("Добавить")
            shortcut: !cancelButton.activeFocus?"return":""
            tooltip: qsTr("Добавить платеж")
            onTriggered: {
                var query1 = "SELECT `bills`.`id`,`bills`.`deposit` FROM `bills`, `users`
                              WHERE `users`.`uid`=" + uid + " AND `users`.`bill_id`=`bills`.`id`;"
                var res1 = dBases.variantListFromDb(query1)
                var query2 = "SELECT SUBSTRING_INDEX(USER(),'@',-1);"
                var res2 = dBases.listFromDb(query2)
                var query3 = "INSERT INTO `payments`
                            (`date`, `sum`, `dsc`, `ip`, `last_deposit`, `uid`, `aid`, `method`,
                             `bill_id`, `inner_describe`, `amount`, `reg_date`, `payment_account`)
                             VALUES ('" +
calendarBox.displayText + "', " +
Number.fromLocaleString(Qt.locale(), paySum.text.toString()) + ", '" +
describeField.text + "',
                             inet_aton('" + res2[0] + "'), " +
res1[0][1] + ", " +
uid + ", " +
rootWindow.userId + ", " +
rootWindow.paymentMethodModel.get(methodBox.currentIndex).num + ", " +
res1[0][0] + ", '" +
(innerDescribeBox.editText.trim() + " " + rootWindow.packAlias + packField.text).trim() + "', " +
Number.fromLocaleString(Qt.locale(), paySum.text.toString()) + ", " +
"now(), " +
companyBox.currentIndex + ");
                             UPDATE `bills` SET `deposit`=(`deposit`+" +
Number.fromLocaleString(Qt.locale(), paySum.text.toString()) +
") WHERE  `id`=" + res1[0][0] + ";"
                dBases.listFromDb(query3)

                addPaymentDialog.refreshPayment()
                addPaymentDialog.paymentAdded()
                addPaymentDialog.visible = false

            }
        }
        Action {
            id: add2Action
            enabled: addPaymentDialog.visible && paySum.acceptableInput
            shortcut: !cancelButton.activeFocus?"enter":""
            onTriggered: {
                addAction.trigger()
            }
        }
        Action {
            id: cancelAction
            enabled: addPaymentDialog.visible
            text: qsTr("Отмена")
            shortcut: "esc"
            tooltip: qsTr("Закрыть окно без ввода платежа")
            onTriggered: {
                addPaymentDialog.visible = false
            }
        }
    }
    onVisibleChanged: {
        if(visible){
            paySum.text = ""
            paySum.forceActiveFocus()
            paySum.cursorPosition = 0
        } else {
            calendarBox.calendarVisible = false
        }
    }
}
