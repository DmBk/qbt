import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1

Item {
    id: colorSettings    
//    ColorDialog {
//        id: colorDialog
//        onAccepted: {
//            console.log("colordialog accepted", colorTableView.currentRow, colorDialog.color)
//            colorModel.setProperty(colorTableView.currentRow, "color", colorDialog.color.toString())
//            colorDialog.close();
//        }
//        onRejected: {
//            console.log("colordialog rejected")
//            colorDialog.close();
//        }
//    }
    TableView {
        id: colorTableView
        property int currentRow: -1
        property alias colorList: colorModel
        headerVisible: false
        implicitWidth: 240
        implicitHeight: 200
        TableViewColumn {
            role: "name"
            delegate: Rectangle {
                color: colorModel.get(styleData.row).color
                Text {
                    text: colorModel.get(styleData.row).name
                }
            }
        }
        TableViewColumn {
            role: "button"
            width: 80
            delegate: Button {
                text: colorModel.get(styleData.row).color
                //                    width: 8
                //                    iconSource: "images/sort-up.png"
                onClicked: {
                    var colorDialog = Qt.createQmlObject(
                        'import QtQuick 2.5; import QtQuick.Controls 1.4;import QtQuick.Dialogs 1.2;
                         ColorDialog {onAccepted:{colorModel.setProperty(colorTableView.currentRow, "color", color.toString());
                         destroy();}
                         onRejected: {destroy();}}', colorSettings, '');

                    colorTableView.currentRow = styleData.row
                    colorDialog.title = colorModel.get(colorTableView.currentRow).name;
                    colorDialog.color = colorModel.get(colorTableView.currentRow).color;
                    colorDialog.visible = true;
                }
            }
        }

        model: ListModel {
            id: colorModel
        }
        function getColor(name)
        {
            for(var i=0;i<colorModel.count;++i){
                if(colorModel.get(i).name==name)
                return colorModel.get(i).color;
            }
//            console.log(qsTr("Тип интерфейса для задания цвета не найден"))
//                return "White";
        }
    }    
    PsTabView {
        id: testTabView
        closingTab: false
        nextTab: false
//        implicitWidth: 150
//        implicitHeight: 150
        backgroundColor: colorTableView.getColor("background")||"steelblue"
        tabColor: colorTableView.getColor("tabViewTab")||"lightsteelblue"
        selectedTabColor: colorTableView.getColor("tabViewSelectedTab")||"steelblue"
        anchors {
            left: colorTableView.right
            top: parent.top
        }

        Tab {
            title: "Tab 1"
            Rectangle{
                id: rect1
                border.color: "black"
                border.width: 2
                color: testTabView.backgroundColor
                Text {
                    anchors.centerIn: parent
                    text: qsTr("Сейчас активна вкладка Tab 1")
                }
            }
        }
        Tab {
            title: "Tab 2"
            Rectangle{
                id: rect2
                border.color: "black"
                border.width: 2
                color: testTabView.backgroundColor
                Text {
//                    width: 150
//                    height: 150
                    anchors.centerIn: parent
                    text: qsTr("Сейчас активна вкладка Tab 2")
                }
            }
        }
    }
    RowLayout {
        id: rowLayout
        anchors {
            right: parent.right
            bottom: parent.bottom
            margins: rootWindow.margins
        }        
        Button {
            id: applyButton
            text: qsTr("Применить")
            Keys.onReturnPressed: clicked()
            Keys.onEnterPressed: clicked()
            onClicked: {
                if(colorTableView.colorList.count<rootWindow.colorList.count)
                    rootWindow.colorList.clear();
                for(var i=0;i<colorTableView.colorList.count;++i){
                    rootWindow.colorList.set(i,colorTableView.colorList.get(i));
                    rootWindow.configDb.writeParam("color",colorTableView.colorList.get(i).name,
                                        colorTableView.colorList.get(i).color.toString());
                }
            }
        }
        Button {
            id: cancelButton
            text: qsTr("Отмена")
            Keys.onReturnPressed: clicked()
            Keys.onEnterPressed: clicked()
            onClicked: {
                if(rootWindow.colorList.count<colorTableView.colorList.count)
                    colorTableView.colorList.clear();
                for(var i=0;i<rootWindow.colorList.count;++i){
                    colorTableView.colorList.set(i,rootWindow.colorList.get(i));
                }
            }
        }
        Button {
            id: restoreToDefaultButton
            text: qsTr("Стандартные настройки")
            Keys.onReturnPressed: clicked()
            Keys.onEnterPressed: clicked()
            onClicked: {                
                if(colorTableView.colorList.count>8)
                    colorTableView.colorList.clear();
                colorTableView.colorList.set(0,{"name":"background", "color":"steelblue"});
                colorTableView.colorList.set(1,{"name":"tabViewTab", "color":"lightsteelblue"});
                colorTableView.colorList.set(2,{"name":"tabViewSelectedTab", "color":"steelblue"});
                colorTableView.colorList.set(3,{"name":"tableViewRow", "color":"white"});
                colorTableView.colorList.set(4,{"name":"tableViewAlternatingRow", "color":"gainsboro"});
                colorTableView.colorList.set(5,{"name":"tableViewSelectedRow", "color":"#0077cc"});
                colorTableView.colorList.set(6,{"name":"tableViewDisableRow", "color":"#fa6052"});
                colorTableView.colorList.set(7,{"name":"tableViewSelectedDisableRow", "color":"#0044aa"});
                colorTableView.colorList.set(8,{"name":"tableViewDeletedRow", "color":"#708090"});
                colorTableView.colorList.set(9,{"name":"tableViewSelectedDeletedRow", "color":"#3a2d71"});
//                colorTableView.modelChanged();
            }

        }
    }
    Component.onCompleted: {
        for(var i=0;i<rootWindow.colorList.count;++i){
            colorTableView.colorList.set(i,rootWindow.colorList.get(i));
        }
//        colorTableView.modelChanged();
    }
}
