﻿import QtQuick 2.5
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.1

ApplicationWindow {
    id: rootWindow
    title: qsTr("qBT - Billing Tools")
    visible: true
    width: 720
    height: 460
    minimumWidth: 200
    minimumHeight: 100
    color: colorList.getColor("tabViewTab")|| "lightsteelblue"
    //    color: "lightskyblue"
    //    opacity: 0.7
//    property color backgroundColor: "steelblue"
    property int margins: 5
    property int fontSizeTop: 12
    property int fontSize1: 11
    property int fontSize2: 10
    property int fontSize3: 9
    property string user
    property string userFio
    property int userId
    property var userPermitPayment    
    property var connectParam
    property bool connectSuccessful: false
    property alias loginDialog: loginDialog
    property alias about: about
    property alias idMenuBar: menuBar.idMenuBar
    property alias statusLabel1: statusLabel1
    property alias statusLabel2: statusLabel2
    property alias statusLabel3: statusLabel3
    property alias statusLabel5: statusLabel5
    property alias statusLabel8: statusLabel8
    property alias statusLabel9: statusLabel9
    property alias statusLabel10: statusLabel10
    property string allValues: "_ALL_VALUES_"
    property string packAlias: "#pack:"    
    property alias configDb: configDb
    property var paymentMethodModel: ListModel {
        function getMethodName(num)
        {
            for(var i=0;i<count;++i){
                if(get(i).num === num)
                return get(i).text;
            }            
        }
    }
    property var arrInnerDescribe: new Object()
    property var colorList: ListModel{
        function getColor(name)
        {
            for(var i = 0; i < count; ++i){
                if(get(i).name === name)
                return get(i).color;
            }
            return undefined;
        }
    }
    signal newTabCreated (string objName)
    signal tabRenameSwitch()
    signal myChild(QtObject qmlItem)

    Component.onCompleted: {        
        width = Math.min(configDb.readParam("window", "width")["width"] || width, Screen.desktopAvailableWidth );
        height = Math.min(configDb.readParam("window", "height")["height"] || height, Screen.desktopAvailableHeight);
        x = (configDb.readParam("window", "x"))["x"] || x;
        y = configDb.readParam("window", "y")["y"] || y;
        if(width==Screen.desktopAvailableWidth&&height-Screen.desktopAvailableHeight>-30) showMaximized();
        loginDialog.visible = true;
        tabView.tabCreated.connect(newTabCreated);
        menuBar.tabRenameSwitch.connect(tabRenameSwitch);
        connectParam = readConnectParam();
        arrInnerDescribe = readInnerDescribes();        
        readPaymentMethod();        
        readColorList();
//        if(colorList.count === 0){
//            colorList.clear();
        if (colorList.getColor("background") === undefined)
            colorList.append({"name":"background", "color":"steelblue"});
        if (colorList.getColor("tabViewTab") === undefined)
            colorList.append({"name":"tabViewTab", "color":"lightsteelblue"});
        if (colorList.getColor("tabViewSelectedTab") === undefined)
            colorList.append({"name":"tabViewSelectedTab", "color":"steelblue"});
        if (colorList.getColor("tableViewRow") === undefined)
            colorList.append({"name":"tableViewRow", "color":"white"});
        if (colorList.getColor("tableViewAlternatingRow") === undefined)
            colorList.append({"name":"tableViewAlternatingRow", "color":"gainsboro"});
        if (colorList.getColor("tableViewSelectedRow") === undefined)
            colorList.append({"name":"tableViewSelectedRow", "color":"#0077cc"});
        if (colorList.getColor("tableViewDisableRow") === undefined)
            colorList.append({"name":"tableViewDisableRow", "color":"#fa6052"});
        if (colorList.getColor("tableViewSelectedDisableRow") === undefined)
            colorList.append({"name":"tableViewSelectedDisableRow", "color":"#0044aa"});
        if (colorList.getColor("tableViewDeletedRow") === undefined)
            colorList.append({"name":"tableViewDeletedRow", "color":"#708090"});
        if (colorList.getColor("tableViewSelectedDeletedRow") === undefined)
            colorList.append({"name":"tableViewSelectedDeletedRow", "color":"#3a2d71"});
//        }
    }

    SqLiteDb {
        id: configDb
    }

    PsMenu {
        id: menuBar
    }

    menuBar: menuBar.idMenuBar

    LoginDialog {
        id: loginDialog
        visible: false
    }    
    About {
        id: about
    }

    PsTabView {
        id: tabView
        anchors.fill: parent
        objectName: "myTabView"
    }

    statusBar: StatusBar {
        RowLayout {
            anchors.fill: parent
            Label {
                id: statusLabel1
                text: qsTr("Не авторизован")
            }
            Label {
                id: statusLabel2
                anchors.left: statusLabel1.right
            }
            Label {
                id: statusLabel3
                anchors.right: statusLabel5.left
            }
            Label {
                id: statusLabel5
                anchors.horizontalCenter: parent.horizontalCenter
            }
            Label {
                id: statusLabel8
                anchors.left: statusLabel5.right
            }
            Label {
                id: statusLabel9
                anchors.right: statusLabel10.left
            }
            Label {
                id: statusLabel10                
                anchors.right: parent.right
            }
        }
    }

    Timer {
        id: timerWindowPosition
        interval: 300
        running: false
        repeat: false
        onTriggered: {
            configDb.writeParam("window", "width", width.toString())
            configDb.writeParam("window", "height", height.toString())
            configDb.writeParam("window", "x", x.toString())
            configDb.writeParam("window", "y", y.toString())
//            console.log(x, y, width, height)
//            console.log(Screen.width, Screen.height, Screen.desktopAvailableWidth, Screen.desktopAvailableHeight)
        }
    }

    onWidthChanged: timerWindowPosition.restart()
    onHeightChanged: timerWindowPosition.restart()
    onXChanged: timerWindowPosition.restart()
    onYChanged: timerWindowPosition.restart()

//    Component.onDestruction: {
//        console.log("main destruction")
////        for(var i=0;i<tabView.count;++i){
////            tabView.closeButtonClicked(0)
////        }
//    }

    function printChild(item) {
        const children = item.children;
        for (var i = children.length; --i >= 0; ) {
            var child = children[i];
            console.log("Child: ", child)
            printChild(child)
        }
    }

    function testInvoke() {
        console.log(dBases.list);
    }

    function readConnectParam()
    {
        var param = configDb.readParam("connect_param");
        if(param["userName"] !== undefined)
            param["userName"] = Qt.atob(loginDialog.encText(param["userName"]));
        if(param["password"] !== undefined)
            param["password"] = loginDialog.encText(param["password"],param["userName"]);
        if(param["keyToDb"] !== undefined)
            param["keyToDb"] = loginDialog.encText(param["keyToDb"],param["userName"]);

//        console.log("param[userName]",param["userName"]);
//        console.log("param[password]",Qt.atob(param["password"]));
//        console.log("param[keyToDb]",Qt.atob(param["keyToDb"]));

        return param;
    }

    function readInnerDescribes()
    {
        var arr1 = configDb.readParam("inner_describe")
        var arr2 = new Array()

        for(var i in arr1){
//            console.log(i)
            arr2.push(i)
        }
        return arr2
    }

    function readPaymentMethod()
    {
        paymentMethodModel.clear();
        var methods = configDb.readParam("payment_method");
        for(var i in methods){
            paymentMethodModel.append({"text":i, "num":Number(methods[i])});
        }
    }

    function readColorList()
    {
        colorList.clear();
        var colors = configDb.readParam("color");
//        if(colors["tableViewDeletedRow"] === undefined){
//            configDb.writeParam("color", "tableViewDeletedRow", "#708090")
//            colors["tableViewDeletedRow"] = "#708090"
//        }
//        if(colors["tableViewSelectedDeletedRow"] === undefined){
//            configDb.writeParam("color", "tableViewSelectedDeletedRow", "#3a2d71")
//            colors["tableViewSelectedDeletedRow"] = "#3a2d71"
//        }
        for(var i in colors){
            colorList.append({"name":i, "color":colors[i]});
        }        
    }
}
