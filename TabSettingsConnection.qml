import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1

Item {
    id: connectionSettings
    signal saveSettings()
    ScrollView {
        id: scrollView
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
            bottom: rowLayout.top
            margins: rootWindow.margins
        }

        GridLayout {
            columns: 2
            Label {
                text: qsTr("Сервер:")
            }
            TextField {
                id: hostName
            }
            Label {
                text: qsTr("Порт:")
            }
            TextField {
                id: port
                validator: IntValidator {bottom: 1; top: 65535;}
            }
            Label {
                text: qsTr("База данных:")
            }
            TextField {
                id: dbName
            }
            Label {
                text: qsTr("Имя пользователя БД:")
            }
            TextField {
                id: userName
            }
            Label {
                text: qsTr("Пароль БД:")
            }
            TextField {
                id: password
                echoMode: TextInput.Password
            }
            Label {
                text: qsTr("Секретный ключ:")
            }
            TextField {
                id: secretKey
                echoMode: TextInput.Password
            }
        }
    }
    RowLayout {
        id: rowLayout
        anchors {
            right: parent.right
            bottom: parent.bottom
            margins: rootWindow.margins
        }
        Button {
            id: applyButton
            text: qsTr("Применить")
            onClicked: {
                connectionSettings.saveSettings()
            }
        }
        Button {
            id: cancelButton
            text: qsTr("Отмена")
        }
        Button {
            id: restoreToDefaultButton
            text: qsTr("Стандартные настройки")
        }
    }
    Component.onCompleted: {
        hostName.text = rootWindow.connectParam["hostName"] || "";
        port.text = rootWindow.connectParam["port"] || "";
        dbName.text = rootWindow.connectParam["dbName"] || "";
        userName.text = rootWindow.connectParam["userName"] || "";
        password.text = Qt.atob(rootWindow.connectParam["password"]) || "";
        secretKey.text = Qt.atob(rootWindow.connectParam["keyToDb"]) || "";
    }
    onSaveSettings: {        
        rootWindow.configDb.writeParam("connect_param", "hostName", hostName.text);
        rootWindow.configDb.writeParam("connect_param", "port", port.text);
        rootWindow.configDb.writeParam("connect_param", "dbName", dbName.text);
        rootWindow.configDb.writeParam("connect_param", "userName",
                                           rootWindow.loginDialog.encText(Qt.btoa(userName.text)));
        rootWindow.configDb.writeParam("connect_param", "password",
                                           rootWindow.loginDialog.encText(Qt.btoa(password.text),userName.text));
        rootWindow.configDb.writeParam("connect_param", "keyToDb",
                                           rootWindow.loginDialog.encText(Qt.btoa(secretKey.text),userName.text));
        rootWindow.connectParam = rootWindow.readConnectParam();
    }
}
