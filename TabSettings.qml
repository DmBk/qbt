import QtQuick 2.5
import QtQuick.Controls 1.4

PsTabView {
    closingTab: false
    nextTab: false
    Component.onCompleted: {        
        var newTab
        var component = Qt.createComponent("TabSettingsCatalog.qml")        
        if (component.status == Component.Ready) {
            newTab = addTab(qsTr("Справочники"), component);
            console.log("Component.Ready")
//            tabView.currentIndex = tabView.count -1
        } else {
            console.log("Component.NotReady")
        }
        var component = Qt.createComponent("TabSettingsInterface.qml")
        if (component.status == Component.Ready) {
            newTab = addTab(qsTr("Интерфейс"), component);
            console.log("Component.Ready")
//            tabView.currentIndex = tabView.count -1
        } else {
            console.log("Component.NotReady")
        }
        component = Qt.createComponent("TabSettingsConnection.qml")
                if (component.status == Component.Ready) {
                    newTab = addTab(qsTr("Соединение"), component);
                    console.log("Component.Ready")
        //            tabView.currentIndex = tabView.count -1
                } else {
                    console.log("Component.NotReady")
                }
    }    
}

