#include "pssqltablemodel.h"

PsSqlTableModel::PsSqlTableModel(QObject *parent) :
    QSqlRelationalTableModel(parent)
{    
    qDebug() << "PsSqlTableModel()";
//    setJoinMode(QSqlRelationalTableModel::LeftJoin);
}

PsSqlTableModel::~PsSqlTableModel()
{
    qDebug() << "~PsSqlTableModel()";
    qDebug() << filter() << orderByClause();
    qDebug() << selectStatement();

}

QVariant PsSqlTableModel::data ( const QModelIndex & index, int role ) const
{
    if(index.row() >= rowCount()) {
        return QString("");
    }
    if(role < Qt::UserRole)
    {
        return QSqlRelationalTableModel::data(index, role);
    }
        else {
    // search for relationships
//            int nbCols = this->columnCount();
//            for (int i = 0; i < nbCols; ++i) {
//                if (this->relation(i).isValid()) {
//        if (this->relation(role - Qt::UserRole - 1).isValid()) {
//            qDebug() << "relation column" << role - Qt::UserRole - 1 << record(index.row()).value(QString(roles.value(role)));
//            return record(index.row()).value(QString(roles.value(role)));
//        }
//            }
//     if no valid relationship was found
//            qDebug() << "no relation" << QSqlQueryModel::data(this->index(index.row(), role - Qt::UserRole - 1), Qt::DisplayRole);
    return QSqlRelationalTableModel::data(this->index(index.row(), role - Qt::UserRole - 1), Qt::DisplayRole);
        }
}

void PsSqlTableModel::generateRoleNames()
{
    roles.clear();    
    for (int i=0; i < m_realColumnCount; ++i) {
//    for (int i=0; i < columnCount(); ++i) {
//        roles[Qt::UserRole + i + 1] = QVariant(this->headerData(i, Qt::Horizontal).toString()).toByteArray();
        qDebug() << "this->record(0).fieldName(i)=" << this->record(0).fieldName(i);
        if (!this->relation(i).isValid()){
            roles[Qt::UserRole + i + 1] = this->headerData(i, Qt::Horizontal).toByteArray();
//            roles[Qt::UserRole + i + 1] = QVariant(this->record(0).fieldName(i)).toByteArray();
        } else {
            qDebug() << i << this->relation(i).displayColumn() << this->relation(i).indexColumn() <<
                        this->relation(i).tableName();
            roles[Qt::UserRole + i + 1] = QVariant(this->relation(i).tableName() + "."
                                                   + this->relation(i).displayColumn()).toByteArray();
        }        
    }
//    qDebug()<< "m_virtualColumn.length()=" << m_virtualColumn.length();
    for (int i=0;i<m_virtualColumn.length();++i){
        if (!this->relation(m_realColumnCount+i).isValid()){
            roles[Qt::UserRole + m_realColumnCount + i + 1] = QVariant(m_virtualColumn.at(i)).toByteArray();
            qDebug() << "virtualColumn not relation";
        } else {
            qDebug() << "virtualColumn relation";
            qDebug() << i << this->relation(m_realColumnCount+i).displayColumn() <<
                        this->relation(m_realColumnCount+i).indexColumn() << this->relation(m_realColumnCount+i).tableName();
            roles[Qt::UserRole + m_realColumnCount + i + 1] = QVariant(this->relation(m_realColumnCount+i).tableName() + "."
                                                   + this->relation(m_realColumnCount+i).displayColumn()).toByteArray();
        }
    }
    qDebug() << "columnCount=" << columnCount();

}

//void PsSqlTableModel::generateColumn(QObject &qmlObject)
//{
//    int nbCols = this->columnCount();
//    for (int i = 0; i < nbCols; ++i) {
//        QMetaObject::invokeMethod(&qmlObject,"psAddColumn", Q_ARG(QVariant, QVariant::fromValue(roles[Qt::UserRole + i + 1])),
//                Q_ARG(QVariant, QVariant::fromValue(roles[Qt::UserRole + i + 1])));

//    }
//}

int PsSqlTableModel::sortColumn() const
{
    return m_sortColumn;
}

void PsSqlTableModel::setSortColumn(int column)
{
    qDebug() << "columnCount="<< columnCount();
    m_sortColumn = column;
    this->setSort(column, m_sortOrder);
}

Qt::SortOrder PsSqlTableModel::sortOrder() const
{
    return m_sortOrder;
}

void PsSqlTableModel::setSortOrder(const Qt::SortOrder order)
{
    m_sortOrder = order;
//    this->setSort(m_sortColumn, order);
}

QString PsSqlTableModel::table() const
{
    return this->tableName();
}

void PsSqlTableModel::setTable(const QString &name)
{
    QSqlRelationalTableModel::setTable(name);
    m_realColumnCount = QSqlRelationalTableModel::columnCount();
    m_virtualColumn.clear();
}

int PsSqlTableModel::columnCount(const QModelIndex &parent) const
{
    return m_realColumnCount + m_virtualColumn.length();
}

QStringList PsSqlTableModel::columnList()
{
    QStringList list;
    int nbCols = roles.size();// this->columnCount();
    for (int i = 0; i < nbCols; ++i) {
        list << QString().fromStdString(roles[Qt::UserRole + i + 1].toStdString());
    }
    qDebug() << list;
    return list;

}


void PsSqlTableModel::setRelationTable(int column, const QString &table, const QString &indexCol, const QString &displayCol)
{
    setRelation(column, QSqlRelation(table, indexCol, displayCol));
}

QString PsSqlTableModel::selectStatement() const
{
    QString str = QSqlRelationalTableModel::selectStatement();
    if (m_virtualColumn.length()>0){
        int selectEndIndex = str.indexOf("` FROM");
        QString separator = "," + this->tableName() + ".`";
                str.insert(selectEndIndex+1, separator + m_virtualColumn.join("`" + separator) + "`" );
    }
    return str;
}

void PsSqlTableModel::addVirtualColumn(const QString &name)
{
    if (setHeaderData(columnCount(),Qt::Horizontal,QVariant(name))){
        qDebug() << "setHeaderData(columnCount(),Qt::Horizontal,QVariant(name)) true";
    } else {
        qDebug() << "setHeaderData(columnCount(),Qt::Horizontal,QVariant(name)) false";
    }
    m_virtualColumn.append(name);    
}
