﻿import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.2

Item {
    id: colorSettings
    ColorDialog {
        id: colorDialog
    }
    TabView {
        TableView {
            id: colorTableView
            property alias colorList: colorModel
            property int clickedRow: -1
            //                headerVisible: false
            //        TableViewColumn {
            //            role: "button"
            //            width: 8
            //            delegate: Button {
            //                width: 8
            //                iconSource: "images/sort-up.png"
            //                onClicked: {
            //                    innerDescribeList.upItem(styleData.row);
            //                    innerDescribeList.modelChanged()
            //                }
            //            }
            //        }
            TableViewColumn {
                role: "name"
                delegate: Rectangle {
                    color: colorModel.get(styleData.row).color
                }
            }
            model: ListModel {
                id: colorModel
            }

//            rowDelegate: Rectangle {
//                color: !styleData.selected?
//                           (styleData.alternate&&innerDescribeList.alternatingRowColors?
//                                "gainsboro":"white"):"#0077cc"
//            }
        }
    }
    RowLayout {
        id: rowLayout
        anchors {
            right: parent.right
            bottom: parent.bottom
            margins: rootWindow.margins
        }
        Button {
            id: applyButton
            text: qsTr("Применить")
            Keys.onReturnPressed: clicked()
            Keys.onEnterPressed: clicked()
            onClicked: {
            }
        }
        Button {
            id: cancelButton
            text: qsTr("Отмена")
            Keys.onReturnPressed: clicked()
            Keys.onEnterPressed: clicked()
            onClicked: {
            }
        }
        Button {
            id: restoreToDefaultButton
            text: qsTr("Стандартные настройки")
            Keys.onReturnPressed: clicked()
            Keys.onEnterPressed: clicked()
            onClicked: {
                colorTableView.colorList.clear();
                colorTableView.colorList.append({"name":"tableViewRow", "color":"white"});
                colorTableView.colorList.append({"name":"tableViewAlternatingRow", "color":"gainsboro"});
                colorTableView.colorList.append({"name":"tableViewSelectedRow", "color":"#0077cc"});
                colorTableView.colorList.append({"name":"tableViewDisableRow", "color":"#fa6052"});
                colorTableView.colorList.append({"name":"tableViewSelectedDisableRow", "color":"#0044aa"});
                colorTableView.colorList.append({"name":"tableViewDeletedRow", "color":"#708090"});
                colorTableView.colorList.append({"name":"tableViewSelectedDeletedRow", "color":"#3a2d71"});
                colorTableView.colorList.append({"name":"background", "color":"steelblue"});
                colorTableView.colorList.append({"name":"tabSelectedTab", "color":"steelblue"});
                colorTableView.colorList.append({"name":"tabNotSelectedTab", "color":"lightsteelblue"});
                colorTableView.modelChanged();
            }

        }
    }
//    Component.onCompleted: {
//    }
}

