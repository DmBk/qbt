import QtQuick 2.5
import QtQuick.Controls 1.4

TableView {
    id: idTableView
    headerVisible: true
    property int columnsWidth: columnWidth()
    property real horizontalScrollBar: __horizontalScrollBar.value    
    property alias timer: idTimer
//    property bool isCtrl: false    
    signal columnMoved()

    Timer {
        id: idTimer
        interval: 1000
        running: false
        repeat: false
        onTriggered: columnMoved()
    }

    Component {
        id: column        
        TableViewColumn { }
//        TableViewColumn {  onWidthChanged: {console.debug("index",__columns)}}
    }

    function psAddColumn(role, title, width) {
        var col1 = addColumn(column)
        col1.title = title || role
        col1.role = role
        if(width!==undefined) {
            col1.width = width
        }
        //            var col2 = addColumn(Qt.createQmlObject('import QtQuick.Controls 1.4; TableViewColumn {}',tableView1,''))
        //            col2.title = "Birthday"
        //            col2.role = "birthday"
        //            var col3 = addColumn(column)
        //            col3.title = Screen.width
        //            col3.role = "disable"
    }

    function columnList() {
        var col = new Array(idTableView.columnCount)
        for(var i=0;i<col.length;++i){
            col[i] = {}
            col[i]["role"] = (idTableView.getColumn(i)).role
            col[i]["title"] = (idTableView.getColumn(i)).title
            col[i]["width"] = (idTableView.getColumn(i)).width
        }
        return col
    }

    function columnWidth() {
            var total=0;
            for(var i=0;i<columnCount;++i){
                total += getColumn(i).width
            }
//            console.debug("columnWidth run")
            return total
    }

    onColumnMoved: {
        console.debug("column moved")
    }

    //    headerDelegate: Rectangle {
    //        width: parent.width
    //        height: 16
    //        border.width: 1
    //        color: "#998787"
    //        border.color: "#e0dbdb"
    //        gradient: Gradient {
    //            GradientStop {
    //                position: 0.52;
    //                color: "#d3d3d3";
    //            }
    //            GradientStop {
    //                position: 0.86;
    //                color: "#ffffff";
    //            }
    //        }
    //        Text {
    //            text: styleData.value
    //            font.pointSize: 9
    //            verticalAlignment: Text.AlignVCenter
    //            horizontalAlignment: Text.AlignHCenter
    //            font.capitalization: Font.Capitalize
    //            anchors.horizontalCenter: parent.horizontalCenter
    ////            color: if (styleData.containsMouse) "white"; else "black";
    //        }

    //    }

    headerDelegate: BorderImage{
            source: "images/header.png"
            border{left:2;right:2;top:2;bottom:2}
            Text {
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                anchors.right: indicator.visible ? indicator.left : parent.right
                anchors.margins: 3
                text: styleData.value
                elide: Text.ElideRight
                font.bold: if (styleData.pressed) {
                               timer.restart()
                               false
                           } else {
                               true
                           }
                color:"#333"
            }
            // Sort indicator
            Image {
                id: indicator
                anchors.verticalCenter: parent.verticalCenter
                anchors.right: parent.right
                anchors.rightMargin: 6
                source: "images/sort-up.png"
                visible: idTableView.sortIndicatorVisible &&
                         styleData.column === idTableView.sortIndicatorColumn
                rotation: idTableView.sortIndicatorOrder === Qt.AscendingOrder ? 0 : 180
                Behavior on rotation { NumberAnimation { } }
            }
//        MouseArea {
//            anchors.fill: parent
//            onClicked: {
//                console.debug("click, mouse.modifiers=", mouse.modifiers)
//                if (mouse.modifiers & Qt.ControlModifier){
//                    idTableView.isCtrl = true
//                    console.debug("control")
//                }
//            }
//            onPressed: {
//                console.debug("pressed, mouse.modifiers=", mouse.modifiers)
//            }
//            onReleased: {
//                console.debug("released, mouse.modifiers=", mouse.modifiers)
//            }
//        }
    }

    itemDelegate: itmDelegate

        Component {
        id : itmDelegate
        Text {            
            width: parent.width
            anchors.margins: 4
            anchors.left: parent.left
            anchors.verticalCenter: parent.verticalCenter
//            elide: styleData.elideMode
            elide: Text.ElideRight
//            wrapMode: Text.WordWrap
            clip: true
            font.pointSize: rootWindow.fontSize3
            //text: styleData.value //styleData.role
            //text: if (styleData.role == "regdate") styleData.value.toLocaleString(Locale.ShortFormat); else styleData.value
            //text: if (typeof(styleData.value) == "object") styleData.value.toLocaleString(Locale.ShortFormat); else styleData.value
            text: if (styleData.value instanceof Date)
                      styleData.value.toLocaleString(Qt.locale(), "yyyy-MM-dd HH:mm:ss")
                  else if (styleData.role != "password")
                      styleData.value
                  else "******"
            //font.capitalization: Font.Capitalize
            //text: "Index: " + model.index
            //text: if (styleData.value.isDate() == true) styleData.value; else styleData.value
            //visible: if (styleData.role == "regdate")  false; else true
            color: styleData.textColor
            //color: if (styleData.column == 3) "red"
        }        

    }

//    rowDelegate: Rectangle {
//        id: idRowDelegate
//        color: "red"
//    }

}
