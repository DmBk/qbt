import QtQuick 2.5
import QtQuick.Controls 1.4

Item {
    id: itemMaxContract
    //    anchors.fill: parent
    objectName: "maxContract"
    property int margins: 5
    Label {
        id: idLabel
        text: qsTr("Последние и неиспользуемые договоры")
        font.pointSize: 12
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top        
    }
    TextArea {
        id: idText
        font.pointSize: 11
        readOnly: true
        backgroundVisible: false        
        horizontalAlignment: Qt.AlignJustify
        anchors {            
            top: idLabel.bottom
            right: parent.horizontalCenter
            margins: rootWindow.margins
        }        
    }
    TextArea {
        id: idText2
        font.pointSize: 11
        readOnly: true
        backgroundVisible: false
        horizontalAlignment: Qt.AlignJustify
        anchors {
            top: idLabel.bottom
            left: parent.horizontalCenter
            margins: rootWindow.margins
        }
    }
    Button {
//        text: qsTr("Обновить")
        anchors {
            right: idText2.right
            top: parent.top
            margins: rootWindow.margins
            topMargin: 0
            rightMargin: 0
        }
        focus: true
        action: refreshAction
        Keys.onReturnPressed: refreshAction.trigger() //parent.orderDogovor()
        Keys.onEnterPressed: refreshAction.trigger() //parent.orderDogovor()
    }

    WorkerScript {
        id: freeDogovorScript
        source: "freeDogovor.js"
        onMessage: idText2.text = messageObject.result
    }

    Component.onCompleted: {
        freeDogovor();
        orderDogovor();        
    }

    function orderDogovor()
    {
        var query = "SELECT contract_id, fio, CONCAT(city, ',', address_street, ',', address_build, ',', address_flat) as address
                      FROM `users_pi`
                      ORDER BY CONVERT(SUBSTRING_INDEX(SUBSTRING_INDEX(contract_id,'/',-1),'-',-1), UNSIGNED) DESC
                      LIMIT 0, 100";
        query = dBases.variantListFromDb(query);
        var contracts = "";
        for (var i=0; i<query.length; ++i) {
            contracts += query[i][0] + " " + query[i][1] + " " + query[i][2] + "\n";
        }
        idText.text = contracts;
        idText.cursorPosition = 0;        
        itemMaxContract.widthChanged();
        itemMaxContract.heightChanged();
    }

    function freeDogovor()
    {
        var query = "SELECT MAX(CONVERT(SUBSTRING_INDEX(SUBSTRING_INDEX(contract_id,'/',-1),'-',-1), UNSIGNED)) AS num
                     FROM `users_pi`";
        var max = dBases.listFromDb(query)[0];
        query = "SELECT DISTINCT CONVERT(SUBSTRING_INDEX(SUBSTRING_INDEX(contract_id,'/',-1),'-',-1), UNSIGNED) AS num
                     FROM `users_pi`
                     HAVING num > 0
                     ORDER BY num";
    //                     LIMIT 0, 2000";
        query = dBases.listFromDb(query);
        idText2.text = "";
        freeDogovorScript.sendMessage({'dogovorList': query, 'max': max})
    }

//    function freeDogovor()
//    {
////        var begin = 0;
////        var period = 1000;
//        var query = "SELECT MAX(CONVERT(contract_id, UNSIGNED)) AS num
//                     FROM `users_pi`";
//        var max = dBases.listFromDb(query)[0];
//        query = "SELECT DISTINCT CONVERT(contract_id, UNSIGNED) AS num
//                     FROM `users_pi`
//                     HAVING num > 0
//                     ORDER BY num";
////                     LIMIT 0, 2000";
//        idText2.text = "";
//        query = dBases.listFromDb(query);
//        for(var i = 1, j = 0; (i <= max) && (j < query.length); ++i){
//            if(i == query[j]){
//                ++j;
//            } else if(i < query[j]){
////                var query2 = "SELECT COUNT(id)
////                              FROM users
////                              WHERE id LIKE 'user" + i + "'";
////                if(dBases.listFromDb(query2)[0] == 0)
//                    idText2.append(i);
//            }
//        }
//        idText2.cursorPosition = 0;
//    }

    onHeightChanged: {
        idText.height = Math.min(Math.max(idText.contentHeight + 2, idText.height),
                                 itemMaxContract.height - idLabel.height - 20);
        idText2.height = Math.min(Math.max(idText2.contentHeight + 2, idText2.height),
                                  itemMaxContract.height - idLabel.height - 20);
        if(idText2.height > idText.height)
            idText.height = idText2.height;
        else
            idText2.height = idText.height;
    }

    onWidthChanged: {
        idText.wrapMode = TextEdit.NoWrap;
        idText.width = Math.min(Math.max(idText.contentWidth + 2, idText.width),
                                itemMaxContract.width / 2 - 20);
        idText.wrapMode = TextEdit.WordWrap;
        idText2.width = Math.min(Math.max(idText2.contentWidth + 2, idText2.width),
                                 itemMaxContract.width / 2 - 20);
        if(idText.width > idText2.width)
            idText2.width = idText.width;
        else
            idText.width = idText2.width;
    }

    onVisibleChanged: {
        if(visible){
            orderDogovor();
            freeDogovor();
        }
    }

    Action {
        id: refreshAction
        text: qsTr("Обновить") + " " + shortcut
        enabled: idText.visible
        onTriggered: {
            orderDogovor();
            freeDogovor();
        }
        shortcut: "F5"
        tooltip: qsTr("Обновить")
    }
}

