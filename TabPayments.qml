import QtQuick 2.5
import QtQuick.Controls 1.4
import PsSqlQueryModel 0.7

Item {
    id: itemPayments
    objectName: "objTabPayments"
    PsTableView {
        id: paymentsView
        signal filterChanged(string role, string value)
        signal searchChanged(string role, string value, bool isDate)
//        signal clearSearch()
        anchors {
            top: parent.top
            right: parent.right
            left: parent.left
            bottom: searchBar.visible? searchBar.top:parent.bottom
//            bottom: parent.bottom
        }
        TableViewColumn { role: "id"; title: qsTr("#"); width: 45 }
        TableViewColumn { role: "uid"; title: qsTr("Л/С"); width: 45 }
        TableViewColumn { role: "date"; title: qsTr("Дата"); width: 65 }
        TableViewColumn { role: "sum"; title: qsTr("Сумма"); width: 45 }
        TableViewColumn { role: "last_deposit"; title: qsTr("Депозит"); width: 60 }
        TableViewColumn { role: "dsc"; title: qsTr("Описание"); width: 80 }
        TableViewColumn { role: "inner_describe"; title: qsTr("Внутреннее"); width: 120 }
        TableViewColumn { role: "reg_date"; title: qsTr("Регистрация"); width: 110 }
        TableViewColumn { role: "method"; title: qsTr("Вид"); width: 60 }
        TableViewColumn { role: "name_account"; title: qsTr("Счет"); width: 60 }
        TableViewColumn { role: "name"; title: qsTr("Администратор"); width: 120; }
        TableViewColumn { role: "city"; title: qsTr("Город"); width: 90 }
        TableViewColumn { role: "address_street"; title: qsTr("Улица"); width: 90 }
        TableViewColumn { role: "address_build"; title: qsTr("Дом"); width: 35 }
        TableViewColumn { role: "address_flat"; title: qsTr("Квартира"); width: 35 }
        model: PsSqlQueryModel {
            id: paymentsModel
            selectString: "SELECT payments.id, payments.date, payments.sum, payments.last_deposit,
                                      payments.dsc, payments.inner_describe, payments.method,
                                      payments.reg_date, payment_accounts.name_account, payments.ext_id,
                                      payments.bill_id, payments.uid, INET_NTOA(payments.ip), admins.name,
                                      payments.currency, payments.amount, users_pi.city, users_pi.address_street,
                                      users_pi.address_build, users_pi.address_flat
                               FROM `payments`
                               LEFT JOIN admins ON payments.aid=admins.aid
                               LEFT JOIN payment_accounts ON payments.payment_account=payment_accounts.id
                               LEFT JOIN users_pi ON users_pi.uid=payments.uid"
            strictFilter: true
        }
        itemDelegate: Text {
//            width: parent.width
            anchors.margins: 4
//            anchors.left: parent.left
//            anchors.verticalCenter: parent.verticalCenter
            elide: Text.ElideRight
            clip: true
            text: if (styleData.value instanceof Date){
                      if(styleData.role == "date")
                          styleData.value.toLocaleString(Qt.locale(), "yyyy-MM-dd") || ""
                      else styleData.value.toLocaleString(Qt.locale(), "yyyy-MM-dd HH:mm:ss") || ""
                  } else if(styleData.role == "sum" || styleData.role == "last_deposit")
                      Number(styleData.value).toLocaleString(Qt.locale()) || "0"
                  else if(styleData.role=="method")
                      rootWindow.paymentMethodModel.getMethodName(Number(styleData.value)) || ""
                  else styleData.value
            color: styleData.textColor
        }

        rowDelegate: Rectangle {
            color: if(!styleData.selected){
                       styleData.alternate && paymentsView.alternatingRowColors?
                            rootWindow.colorList.getColor("tableViewAlternatingRow"):
                            rootWindow.colorList.getColor("tableViewRow")
                   } else {
                       rootWindow.colorList.getColor("tableViewSelectedRow")
                   }
        }

        Component.onCompleted: {
//            paymentsModel.setFullQuery()
            paymentsView.forceActiveFocus();
            paymentsModel.setMultiRole("uid", "payments");
            paymentsModel.setMultiRole("id", "payments");
            searchBar.fieldTextChanged.connect(paymentsView.filterChanged);
        }

        onSortIndicatorOrderChanged: {
            timer.stop()
            if(!paymentsView.sortIndicatorVisible) {
                paymentsView.sortIndicatorVisible = true
            }

            var column = paymentsView.getColumn(paymentsView.sortIndicatorColumn)
            paymentsModel.setSortRoles(column.role, paymentsView.sortIndicatorOrder)
            paymentsModel.setFullQuery();

        }

        onSortIndicatorColumnChanged: {
            timer.stop()

            if(!paymentsView.sortIndicatorVisible) {
                paymentsView.sortIndicatorVisible = true
            }

            var column = paymentsView.getColumn(paymentsView.sortIndicatorColumn)
            paymentsModel.setSortRoles(column.role, paymentsView.sortIndicatorOrder)
            paymentsModel.setFullQuery();
        }
        onSearchChanged: {
            paymentsModel.setSearchRoles(role, value, isDate);
            paymentsModel.setFullQuery();
            statusWrite();
        }
        onFilterChanged: {
            paymentsModel.setFilterRoles(role, value);
            paymentsModel.setFullQuery();
            statusWrite();
        }
        onHorizontalScrollBarChanged: {
            //            console.debug("onHorizontalScrollBarChanged", horizontalScrollBar)
            searchBar.__horizontalScrollBar.value = horizontalScrollBar
        }
    }

    PsSearchBar {
        id: searchBar
        visible: false
        height: 30
        width: parent.width
        horizontalScrollBarPolicy: Qt.ScrollBarAlwaysOff
        anchors.bottom: parent.bottom
    }

    PaymentsSearchDialog {
        id: paymentsSearchDialog
    }
    Timer {
        id: timerStatus
        interval: 10
        running: false
        repeat: false
        onTriggered: statusWrite()
    }
    onVisibleChanged: {
        if(visible){
            timerStatus.start()
        } else {
            statusClear()
        }
    }

    Action {
        id: searchBarAction
        enabled: paymentsView.visible
        text: !searchBar.visible?qsTr("Фильтр"):qsTr("Убрать фильтр")
        shortcut: "ctrl+f"
        tooltip: qsTr("Показать/Спрятать панель фильтра")
        onTriggered: {
            searchBar.listModel.clear()
            if (!searchBar.visible) {
                var columns = paymentsView.columnList()
                for (var i=0;i<columns.length;++i){
                    searchBar.listModel.append({"elementWidth": columns[i]["width"],
                                                   "elementRole": columns[i]["role"],
                                                   "elementTitle": columns[i]["title"]});
                }
                searchBar.visible = true
                searchBar.listView.forceActiveFocus()
            } else {
                searchBar.visible = false;
                paymentsModel.clearFilter();
                paymentsModel.setFilterRoles();
                paymentsModel.setFullQuery();
                paymentsView.forceActiveFocus();
            }
        }
    }

    Action {
        id: searchAction
        enabled: paymentsView.visible
        text: qsTr("Поиск")
        shortcut: "ins"
        tooltip: qsTr("Открыть диалог поиска")
        onTriggered: {            
            paymentsSearchDialog.visible = paymentsSearchDialog.visible?false:true
        }
    }
    Action {
        id: clearSearchAction
        enabled: paymentsView.visible
        text: qsTr("Очистить параметры поиска")
        shortcut: "-"
        tooltip: qsTr("Отменить параметры поиска")
        onTriggered: {
            paymentsSearchDialog.clearFields()

        }        
    }
    Action {
        id: refreshAction
        enabled: paymentsView.visible
        text: qsTr("Обновить")
        shortcut: "F5"
        tooltip: qsTr("Обновить список")
        onTriggered: {
            paymentsModel.setFullQuery()
            statusWrite()
        }
    }

    MenuItem {
        id: searchMenuItem
        action: searchAction
    }
    MenuItem {
        id: searchBarMenuItem
        action: searchBarAction
    }
    MenuItem {
        id: clearSearchMenuItem
        action: clearSearchAction
    }
    MenuItem {
        id: refreshMenuItem
        action: refreshAction
    }

    Component.onCompleted: {        
        paymentsSearchDialog.fieldTextChanged.connect(paymentsView.searchChanged)        
        rootWindow.idMenuBar.paymentsMenu.visible = true
        rootWindow.idMenuBar.paymentsMenu.insertItem(0,searchMenuItem)
        rootWindow.idMenuBar.paymentsMenu.insertItem(1,searchBarMenuItem)
        rootWindow.idMenuBar.paymentsMenu.insertItem(2,clearSearchMenuItem)
        rootWindow.idMenuBar.paymentsMenu.insertItem(3,refreshMenuItem)
    }

    Component.onDestruction: {        
        statusClear()
        rootWindow.idMenuBar.paymentsMenu.visible = false
        rootWindow.idMenuBar.paymentsMenu.removeItem(searchMenuItem)
        rootWindow.idMenuBar.paymentsMenu.removeItem(searchBarMenuItem)
        rootWindow.idMenuBar.paymentsMenu.removeItem(clearSearchMenuItem)
        rootWindow.idMenuBar.paymentsMenu.removeItem(refreshMenuItem)
    }

    function sumCalc()
    {
        var query = paymentsModel.fullQuery(false, true, true, false)
        query = "SELECT COUNT(*), IFNULL(SUM(sum),0) FROM `payments`
                               LEFT JOIN admins ON payments.aid=admins.aid
                               LEFT JOIN payment_accounts ON payments.payment_account=payment_accounts.id
                               LEFT JOIN users_pi ON users_pi.uid=payments.uid " + query
//        console.log("query=", query)
        return dBases.variantListFromDb(query)
    }

    function statusWrite()
    {
        var calc = sumCalc()
        rootWindow.statusLabel10.text = qsTr("  Количество платежей: ") + (Number(calc[0][0]).toLocaleString(Qt.locale(),"f",0)||0) +
                                        qsTr("  На сумму: ") + (Number(calc[0][1]).toLocaleString(Qt.locale())||0)
    }

    function statusClear()
    {
        rootWindow.statusLabel10.text = ""
    }

}
