import QtQuick 2.5
import QtQuick.Controls 1.4
import PsSqlQueryModel 0.7

Item {
    id: itemContract
    objectName: "objTabContract"
    ComboBox {
        id: comboBox
        anchors {
            top: parent.top
            left: parent.left
            margins: rootWindow.margins
        }
        model: [qsTr("Номер договора число"), qsTr("Номер договора строка")]
    }

}
